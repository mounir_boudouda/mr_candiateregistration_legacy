//
//  UniversityTableViewController.h
//  Candidate Registrations
//
//  Created by Robert Taylor on 26/07/2011.
//  Copyright 2011 Home. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PopOverListItem.h"

@protocol UniversityTableViewControllerDelegate <NSObject>

-(void)didUniversityTap:(PopOverListItem*)item;

@end

@interface UniversityTableViewController : UITableViewController
{
    NSMutableArray *universityListArray;
    __weak id popOverDelegate;
    NSMutableArray *localUniversityListArray;
    int eventId;
    NSMutableArray *searchResult;
    NSString *searchString;
}

@property (nonatomic, weak) id<UniversityTableViewControllerDelegate> popOverDelegate;
@property (nonatomic, strong) NSMutableArray *universityListArray;
@property (nonatomic, strong) NSMutableArray *localUniversityListArray;
@property (nonatomic, assign) int eventId;
@property (nonatomic, copy) NSString *searchString;

- (void) searchTableView:(NSString*)searchText;
- (PopOverListItem*) findUniversity:(int)universityId;
@end

