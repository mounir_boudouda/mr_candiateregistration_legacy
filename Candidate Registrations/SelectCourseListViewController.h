//
//  SelectCourseListViewController.h
//  Candidate Registrations
//
//  Created by Afzal Valiji on 21/08/2012.
//  Copyright 2012 Home. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CourseTableViewController.h"
#import "InputViewKeyboard.h"

@protocol SelectCourseListViewControllerDelegate <NSObject>

-(void)didCourseTap:(PopOverListItem*)item;

@end

@interface SelectCourseListViewController : UIViewController<CourseTableViewControllerDelegate>
{
    __weak id popOverDelegate;
    CourseTableViewController  *tableListController;
    UIView *tableContainerView;
    InputViewKeyboard *textField;
    int eventId;
}


@property (nonatomic, weak) id<SelectCourseListViewControllerDelegate> popOverDelegate;
@property (nonatomic, strong) IBOutlet CourseTableViewController *tableListController;
@property (nonatomic, strong) IBOutlet UIView *tableContainerView;
@property (nonatomic, strong) IBOutlet InputViewKeyboard *textField;
@property (nonatomic, assign) int eventId;

-(void)didCourseTap:(PopOverListItem *)item;
-(void)didEditText;

@end
