//
//  DataUtils.h
//  Candidate Registrations
//
//  Created by Robert Taylor on 17/07/2011.
//  Copyright 2011 Home. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>
#import "CandidateEvent.h"
#import "WebserviceUtils.h"
#import "KeyChain.h"

@interface DataUtils : NSObject {
}

+ (NSString *) dataFilePath;
+ (sqlite3 *) getNewDBConnection;
- (NSMutableArray *) loadCandidateList:(int)eventId;
- (int) getCandidateCounts:(BOOL)isUploaded:(int)eventId;
- (NSMutableArray *) loadEventsList:(BOOL)isArchive;
- (CandidateEvent *) loadEventByEventId:(int)eventId;
- (NSMutableArray *) loadEventLocalUniversityList:(int)eventId;
- (NSMutableArray *) loadEventLocalCourseList:(int)eventId;
- (BOOL) insertEventLocalCourse:(int)eventId:(int)courseId:(int)displayOrder;
- (BOOL) removeEventLocalCourse:(int)eventId:(int)courseId;
- (BOOL) insertEventLocalUniversity:(int)eventId:(int)universityId:(int)displayOrder;
- (BOOL) removeEventLocalUniversity:(int)eventId:(int)universityId;
- (BOOL) syncUniversityList:(NSString*)url;
- (BOOL) syncCourseList:(NSString*)url;
- (BOOL) syncEventList:(NSString*)url;
- (BOOL) saveCourseItem:(PopOverListItem*) courseItem;
- (BOOL) saveUniversityItem:(PopOverListItem*) universityItem;
- (BOOL) saveEventItem:(CandidateEvent*) eventItem;
- (BOOL) deleteEventItem:(CandidateEvent*) eventItem;
- (BOOL) uploadCandidates:(int)eventId;
- (NSMutableArray *) loadUniversityList;
- (NSMutableArray *) loadCourseList;
- (BOOL)saveCandidateRegistration: (int) eventId:(int) universityId:(int) courseId:(NSString*)firstName:(NSString*) lastName:(NSString*) emailAddress: (int)graduationYear;
- (BOOL)updateCandidateDetails:(CandidateDetails*)details;
- (CandidateDetails *)loadCandidate:(int)candidateId;
- (BOOL)saveError:(NSString*) errorDescription;
- (NSMutableArray *)loadErrorList;
- (BOOL)clearErrorList;
- (BOOL)checkPassword:(NSString*)password;
- (NSString*)getProviderId;
- (NSString*)getProviderPassword;
- (NSString*)getServerName;
- (BOOL)setServerName:(NSString*)serverName;
- (BOOL)setProviderId:(NSString*)providerId:(NSString*)providerPassword;
- (void)resetPassword:(BOOL)isITAdmin;
- (BOOL)saveNewPassword:(NSString*)oldPassword:(NSString*)newPassword;
- (BOOL)deleteCourses;
- (BOOL)deleteUniversities;


@end
