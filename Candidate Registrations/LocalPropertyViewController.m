//
//  LocalUniversitiesViewController.m
//  Candidate Registrations
//
//  Created by Robert Taylor on 14/08/2011.
//  Copyright 2011 Home. All rights reserved.
//

#import "LocalPropertyViewController.h"
#import "QuartzCore/QuartzCore.h"

@implementation LocalPropertyViewController
@synthesize tableView;
@synthesize backButton;
@synthesize tableViewController;
@synthesize eventId;
@synthesize isUniversityList;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (IBAction)backButtonClicked:(id)sender
{	
    // get the current view
    UIView *currentView = self.view;
	// get the the underlying UIWindow, or the view containing the current view view
	UIView *theWindow = [currentView superview];
    
    // remove the current view and replace with myView1
	[currentView removeFromSuperview];
    
    // set up an animation for the transition between the views
	CATransition *animation = [CATransition animation];
	[animation setDuration:0.5];
	[animation setType:kCATransitionFade];
	[animation setSubtype:kCATransitionFromLeft];
	[animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
	
	[[theWindow layer] addAnimation:animation forKey:@"SwitchToEventTableView"];
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.view.frame = [[UIScreen mainScreen] applicationFrame];
    
    tableViewController = [[LocalPropertyTableViewController alloc] 
                           initWithNibName:@"LocalPropertyTableViewController" 
                           bundle:nil];
    tableViewController.eventId = eventId;
    tableViewController.isUniversityList = isUniversityList;
    [tableView addSubview:tableViewController.view];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
	return NO;
}

@end
