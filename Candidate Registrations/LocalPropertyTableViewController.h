//
//  LocalUniversitiesTableViewController.h
//  Candidate Registrations
//
//  Created by Robert Taylor on 14/08/2011.
//  Copyright 2011 Home. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LocalPropertyCellView.h"
@interface LocalPropertyTableViewController : UITableViewController
{
    int eventId;
    LocalPropertyCellView *propertyTableCell;
    NSMutableArray *globalPropertyList;
    NSMutableArray *localPropertyList;
    bool isUniversityList;
}
@property (nonatomic, assign) int eventId;
@property (nonatomic, strong) IBOutlet LocalPropertyCellView *propertyTableCell;
@property (nonatomic, assign) bool isUniversityList;
@end
