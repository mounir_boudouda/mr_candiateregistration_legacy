//
//  EventLocalUniversity.h
//  Candidate Registrations
//
//  Created by Robert Taylor on 19/08/2011.
//  Copyright 2011 Home. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EventLocalProperty : NSObject
{
    int eventId;
    int propertyId;
    int rowId;
    int displayOrder;
}

@property(readwrite,assign) int eventId;
@property(readwrite,assign) int propertyId;
@property(readwrite,assign) int rowId;
@property(readwrite,assign) int displayOrder;

@end
