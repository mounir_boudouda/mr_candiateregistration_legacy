//
//  PasswordViewController.h
//  Candidate Registrations
//
//  Created by Robert Taylor on 24/08/2011.
//  Copyright 2011 Home. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol PasswordViewDelegate
- (void)didSelectCancel;
- (void)didValidatePassword;
@end

@interface PasswordViewController : UIViewController
{
    id <NSObject, PasswordViewDelegate> delegate;
    UITextField *passwordTextField;
    UIButton *enterButton;
    UIButton *cancelButton;
    BOOL showCancel;
}

@property (nonatomic, strong) IBOutlet UITextField *passwordTextField;
@property (nonatomic, strong) IBOutlet UIButton *enterButton;
@property (nonatomic, strong) IBOutlet UIButton *cancelButton;
@property (nonatomic, assign) BOOL showCancel;
@property (strong) id <NSObject, PasswordViewDelegate > delegate;

- (IBAction) enterButtonPressed:(id)sender;
- (IBAction) cancelButtonPressed:(id)sender;

@end
