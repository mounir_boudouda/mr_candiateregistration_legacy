//
//  ErrorLogViewController.h
//  Candidate Registrations
//
//  Created by Robert Taylor on 13/08/2011.
//  Copyright 2011 Home. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ErrorLogTableViewController.h"
#import "DataUtils.h"

@interface ErrorLogViewController : UIViewController
{
    ErrorLogTableViewController *tableListController;
    UIView *tableContainerView;
    UIButton *clearListButton;
}

@property (nonatomic, strong) IBOutlet UIButton *clearListButton;
@property (nonatomic, strong) IBOutlet ErrorLogTableViewController *tableListController;
@property (nonatomic, strong) IBOutlet UIView *tableContainerView;

-(IBAction)clearButtonClicked:(id)sender;
@end
