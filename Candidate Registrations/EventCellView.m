//
//  EventCellView.m
//  Candidate Registrations
//
//  Created by Robert Taylor on 23/07/2011.
//  Copyright 2011 Home. All rights reserved.
//

#import "EventCellView.h"
#import "DataUtils.h"

@implementation EventCellView

@synthesize eventDateText;
@synthesize eventTitleText;
@synthesize dateEntryButton;
@synthesize updateButton;
@synthesize candidateEvent;
@synthesize notUploadedLabel;
@synthesize uploadedLabel;
@synthesize totalLabel;
@synthesize eventId;
@synthesize deleteButton;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)initEvent
{
    NSString *rowTitle = candidateEvent.eventName;
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"cccc dd MMMM yyyy"];
    
    NSString *stringFromDate = [formatter stringFromDate:candidateEvent.eventDate];
    eventTitleText.text = rowTitle;  
    eventDateText.text = stringFromDate;
    [self setStatsLabel];
    
}

- (void)setStatsLabel
{
    DataUtils *dataUtils = [[DataUtils alloc] init];
    int uploadedCount = [dataUtils getCandidateCounts:true :eventId];
    int notUploadedCount = [dataUtils getCandidateCounts:false:eventId];
    
    [notUploadedLabel setText:[NSString stringWithFormat:@"%d",notUploadedCount]];
    [uploadedLabel setText:[NSString stringWithFormat:@"%d",uploadedCount]];
    [totalLabel setText:[NSString stringWithFormat:@"%d",notUploadedCount+uploadedCount]];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


@end
