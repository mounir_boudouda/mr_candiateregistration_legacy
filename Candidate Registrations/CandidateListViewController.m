//
//  CandidateListViewController.m
//  Candidate Registrations
//
//  Created by Robert Taylor on 09/08/2011.
//  Copyright 2011 Home. All rights reserved.
//

#import "CandidateListViewController.h"
#import "QuartzCore/QuartzCore.h"
#import "Candidate_RegistrationsAppDelegate.h"
#import "DataUtils.h"

@implementation CandidateListViewController
@synthesize eventId;
@synthesize tableViewController;
@synthesize tableView;
@synthesize backButton;
@synthesize uploadButton;
@synthesize localUniversitiesButton;
@synthesize localPropertyViewController;
@synthesize eventNameLabel;
@synthesize uploadedLabel;
@synthesize notUploadedLabel;
@synthesize totalCountLabel;
@synthesize isDataUploadSuccessful;
@synthesize messageOverlay;
@synthesize delegate;
@synthesize localCoursesButton;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        isDataUploadSuccessful = FALSE;
    }
    return self;
}


- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (IBAction)uploadButtonClicked:(id)sender
{	
    FeedbackMessageOverlay *messageController = [[FeedbackMessageOverlay alloc] 
                                                 initWithNibName:@"FeedbackMessageOverlay" 
                                                 bundle:nil];
    messageController.showCloseButton = FALSE;
    messageController.title = @"Uploading...";
    messageController.message = @"Please wait whilst the candidates are uploaded.";
    self.messageOverlay = messageController;
    [self.view addSubview:messageOverlay.view];

    [NSThread detachNewThreadSelector:@selector(startDataUpload) toTarget:self withObject:nil];
}

-(void) startDataUpload
{
    
    @autoreleasepool {
    
        DataUtils *dataUtils = [[DataUtils alloc] init];
        isDataUploadSuccessful = [dataUtils uploadCandidates:eventId];
        
        [self performSelectorOnMainThread:@selector(dataUploadComplete) withObject:nil waitUntilDone:NO];
    }
    
    
}

-(void) dataUploadComplete
{
    // close this view
    [self.messageOverlay closeOverlay];
    
    if (isDataUploadSuccessful==FALSE)
    {
        FeedbackMessageOverlay *messageController = [[FeedbackMessageOverlay alloc] 
                                                     initWithNibName:@"FeedbackMessageOverlay" 
                                                     bundle:nil];
        messageController.showCloseButton = TRUE;
        messageController.title = @"Oops!";
        messageController.message = @"Data sync failed";            
        self.messageOverlay = messageController;
        [self.view addSubview:messageOverlay.view];
    }
    else
    {
        FeedbackMessageOverlay *messageController = [[FeedbackMessageOverlay alloc] 
                                                     initWithNibName:@"FeedbackMessageOverlay" 
                                                     bundle:nil];
        messageController.showCloseButton = TRUE;
        messageController.title = @"Success!";
        messageController.message = @"Data sync was successful";
        self.messageOverlay = messageController;
        [self.view addSubview:messageOverlay.view];
    }
    isDataUploadSuccessful = FALSE;
    [self reloadView];
}

- (IBAction)localUniversitiesButtonClicked:(id)sender
{	
    [self displayLocalPropertiesScreen:true];
}

- (IBAction)localCoursesButtonClicked:(id)sender
{	
    [self displayLocalPropertiesScreen:false];
}

-(void)displayLocalPropertiesScreen:(bool)isUniversity
{
    localPropertyViewController = [[LocalPropertyViewController alloc] initWithNibName:@"LocalPropertyViewController" bundle:nil];
    
    localPropertyViewController.eventId = eventId;
    localPropertyViewController.isUniversityList = isUniversity;
    
    Candidate_RegistrationsAppDelegate *appDelegate = (Candidate_RegistrationsAppDelegate *)[[UIApplication sharedApplication] delegate];
    
    [appDelegate.window addSubview:localPropertyViewController.view];   
    
    // set up an animation for the transition between the views
	CATransition *animation = [CATransition animation];
	[animation setDuration:0.5];
	[animation setType:kCATransitionFade];
	[animation setSubtype:kCATransitionFromLeft];
	[animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
	
	[[appDelegate.window layer] addAnimation:animation forKey:@"SwitchToViewSelectLocalProperties"];

}


- (IBAction)backButtonClicked:(id)sender
{	
    // get the current view
    UIView *currentView = self.view;
	// get the the underlying UIWindow, or the view containing the current view view
	UIView *theWindow = [currentView superview];

    
    // remove the current view and replace with myView1
	[currentView removeFromSuperview];
    
    // set up an animation for the transition between the views
	CATransition *animation = [CATransition animation];
	[animation setDuration:0.5];
	[animation setType:kCATransitionFade];
	[animation setSubtype:kCATransitionFromLeft];
	[animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
	
	[[theWindow layer] addAnimation:animation forKey:@"SwitchToEventTableView"];
    [delegate candidateListViewDidClose];
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    // to fix the controller showing under the status bar
	self.view.frame = [[UIScreen mainScreen] applicationFrame];
    
    [self reloadView];
    
}

- (void)reloadView
{
    if (tableViewController!=nil)
    {
        if ([tableView subviews]){
            for (UIView *subview in [tableView subviews]) {
                if (subview == tableViewController.view)
                {
                    [subview removeFromSuperview];
                    break;
                }
            }
        }
    }
    
    tableViewController = [[CandidateTableViewController alloc] 
                           initWithNibName:@"CandidateTableViewController" 
                           bundle:nil];
    tableViewController.eventId = eventId;
    
    DataUtils *utils = [[DataUtils alloc]init];
    
    CandidateEvent *event = [utils loadEventByEventId:eventId];
    if (event!=nil && event.rowId>0)
    {
        [eventNameLabel setText:event.eventName];
    }
    
    int uploadedCount = [utils getCandidateCounts:true :eventId];
    int notUploadedCount = [utils getCandidateCounts:false:eventId];
    notUploadedLabel.text = [NSString stringWithFormat:@"%d",notUploadedCount];
    uploadedLabel.text = [NSString stringWithFormat:@"%d",uploadedCount];
    totalCountLabel.text = [NSString stringWithFormat:@"%d",notUploadedCount+uploadedCount];
    
    
    
    [tableView addSubview:tableViewController.view];
   
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
	return NO;
}

@end
