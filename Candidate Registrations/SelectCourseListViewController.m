//
//  SelectCourseListViewController.m
//  Candidate Registrations
//
//  Created by Afzal Valiji on 21/08/2012.
//  Copyright 2012 Home. All rights reserved.
//

#import "SelectCourseListViewController.h"

@implementation SelectCourseListViewController

@synthesize tableListController;
@synthesize tableContainerView;
@synthesize eventId;
@synthesize textField;
@synthesize popOverDelegate;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    tableListController = [[CourseTableViewController alloc] 
                           initWithNibName:@"CourseTableViewController" 
                           bundle:nil];
    tableListController.popOverDelegate = self;
    tableListController.eventId = eventId;
    [tableContainerView addSubview:tableListController.view];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didEditText) name:UITextViewTextDidChangeNotification object:textField];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    tableListController = nil;
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UITextViewTextDidChangeNotification object:textField];
}

- (void)viewWillAppear:(BOOL)animated
{
    [tableListController viewWillAppear:animated];
    [super viewWillAppear:animated];
    [textField becomeFirstResponder];
    [self didEditText];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [textField resignFirstResponder];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
	return YES;
}

-(void)didCourseTap:(PopOverListItem *)item {
    [popOverDelegate didCourseTap:item];
}

-(void)didEditText
{
    [tableListController searchTableView:textField.text];
}

@end
