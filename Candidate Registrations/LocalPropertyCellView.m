//
//  LocalUniversitiesCellView.m
//  Candidate Registrations
//
//  Created by Robert Taylor on 14/08/2011.
//  Copyright 2011 Home. All rights reserved.
//

#import "LocalPropertyCellView.h"

@implementation LocalPropertyCellView

@synthesize onOffSwitch;
@synthesize nameLabel;
@synthesize listItem;
@synthesize localList;
@synthesize eventId;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)initCell
{
    if (listItem!=nil)
    {
        onOffSwitch = [[LocalPropertyUISwitch alloc] initWithFrame:CGRectMake(650, 10, 0, 0)];
        [self.contentView addSubview:onOffSwitch];
        [nameLabel setText:listItem.description];
        
        [onOffSwitch addTarget:self.superview action:@selector(propertySwitchSelected:) forControlEvents:UIControlEventValueChanged];
        
        [onOffSwitch setEventId:eventId];
        [onOffSwitch setPropertyId:listItem.rowId];

        
        EventLocalProperty *local = [self findProperty:listItem.rowId];
        if (local!=nil)
        {
            [onOffSwitch setOn:true];
        }
        else
        {
            [onOffSwitch setOn:false];
        }
    }
    else
    {
        [nameLabel setText:@""];
    }
}

- (EventLocalProperty*) findProperty:(int)propertyId
{
    int loop=0;
    for (loop=0; loop<localList.count; loop++)
    {
        EventLocalProperty *detail = (EventLocalProperty *)[localList objectAtIndex:loop];
        if (detail!=nil && detail.propertyId==propertyId)
        {
            return detail;
        }
    }
    return nil;
}


@end
