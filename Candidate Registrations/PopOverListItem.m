//
//  PopOverListItem.m
//  Candidate Registrations
//
//  Created by Robert Taylor on 26/07/2011.
//  Copyright 2011 Home. All rights reserved.
//

#import "PopOverListItem.h"

@implementation PopOverListItem

@synthesize rowId;
@synthesize description;

- (id)init
{
    self = [super init];
    if (self) {
        // Initialization code here.
    }
    
    return self;
}


@end
