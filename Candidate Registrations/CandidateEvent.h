//
//  CandidateEvent.h
//  Candidate Registrations
//
//  Created by Robert Taylor on 17/07/2011.
//  Copyright 2011 Home. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface CandidateEvent : NSObject {
    int rowId;
    NSString *eventName;
    int eventId;
    NSDate *eventDate;
}

@property(nonatomic,copy) NSString *eventName;
@property(nonatomic,copy) NSDate *eventDate;
@property(readwrite,assign) int rowId;
@property(readwrite,assign) int eventId;

@end
