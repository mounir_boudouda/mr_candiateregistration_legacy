//
//  EventLocalUniversity.m
//  Candidate Registrations
//
//  Created by Robert Taylor on 19/08/2011.
//  Copyright 2011 Home. All rights reserved.
//

#import "EventLocalProperty.h"

@implementation EventLocalProperty
@synthesize eventId;
@synthesize propertyId;
@synthesize rowId;
@synthesize displayOrder;

- (id)init
{
    self = [super init];
    if (self) {
        // Initialization code here.
    }
    
    return self;
}

@end
