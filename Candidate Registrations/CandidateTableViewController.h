//
//  CandidateTableViewController.h
//  Candidate Registrations
//
//  Created by Robert Taylor on 10/08/2011.
//  Copyright 2011 Home. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CandidateCellView.h"
#import "AdminEditDataViewController.h"

@interface CandidateTableViewController : UITableViewController <EditDataViewDelegate>
{
    NSMutableArray *candidatesList;
    int eventId;
    CandidateCellView *candidateTableCell;
    AdminEditDataViewController *adminEditDataViewController;
}

@property (nonatomic, assign) int eventId;
@property (nonatomic, strong) IBOutlet CandidateCellView *candidateTableCell;
-(IBAction)editButtonPressed:(id)sender;
@end
