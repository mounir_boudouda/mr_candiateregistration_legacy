//
//  DataEntryViewController.h
//  Candidate Registrations
//
//  Created by Robert Taylor on 23/07/2011.
//  Copyright 2011 Home. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SelectUniversityListViewController.h"
#import "SelectCourseListViewController.h"
#import "GraduationYearTableViewController.h"
#import "InputViewKeyboard.h"
#import "PopOverListItem.h"
#import "FeedbackMessageOverlay.h"
#import "PasswordViewController.h"
#import "Constants.h"

@protocol DataEntryViewDelegate
- (void)dataEntryViewDidClose;
@end

@interface DataEntryViewController : UIViewController<SelectCourseListViewControllerDelegate, SelectUniversityListViewControllerDelegate, GraduationYearPopOverControllerDelegate, UITextViewDelegate, PasswordViewDelegate>
{
    UIImageView *backgroundImageView;
    UIButton *closeButton;
    UIButton *registerButton;
    UIButton *universityButton;
    UIButton *courseButton;
    UIButton *graduationButton;
    UIPopoverController *universityPopOverController;
    UIPopoverController *coursePopOverController;
    UIPopoverController *graduationYearPopOverController;
    SelectUniversityListViewController *universityListController;    
    SelectCourseListViewController *courseListController;  
    GraduationYearTableViewController *graduationYearListController;
    InputViewKeyboard *firstNameKeyboardView;
    InputViewKeyboard *lastNameKeyboardView;
    InputViewKeyboard *emailNameKeyboardView;
    int eventId;
    PopOverListItem *defaultUniversityPopOverItem;
    PopOverListItem *defaultCoursePopOverItem;
    PopOverListItem *defaultGraduationYearPopOverItem;
    FeedbackMessageOverlay *messageOverlay;
    UILabel *eventNameLabel;
    UILabel *eventStatsLabel;
    PasswordViewController *passwordController;
    id <NSObject, DataEntryViewDelegate> delegate;
    
}

@property (nonatomic, strong) IBOutlet UIImageView *backgroundImageView;
@property (nonatomic, strong) IBOutlet UIButton *closeButton;
@property (nonatomic, strong) IBOutlet UIButton *registerButton;
@property (nonatomic, strong) IBOutlet UIButton *universityButton;
@property (nonatomic, strong) IBOutlet UIButton *courseButton;
@property (nonatomic, strong) IBOutlet UIButton *graduationButton;
@property (nonatomic, strong) UIPopoverController *universityPopOverController;
@property (nonatomic, strong) UIPopoverController *coursePopOverController;
@property (nonatomic, strong) UIPopoverController *graduationYearPopOverController;
@property (nonatomic, strong) IBOutlet SelectUniversityListViewController *universityListController;
@property (nonatomic, strong) IBOutlet SelectCourseListViewController *courseListController;
@property (nonatomic, strong) IBOutlet GraduationYearTableViewController *graduationYearListController;
@property (nonatomic, strong) IBOutlet InputViewKeyboard *firstNameKeyboardView;
@property (nonatomic, strong) IBOutlet InputViewKeyboard *emailNameKeyboardView;
@property (nonatomic, strong) IBOutlet InputViewKeyboard *lastNameKeyboardView;
@property (nonatomic, assign) int eventId;
@property (nonatomic, strong) PopOverListItem *defaultCoursePopOverItem;
@property (nonatomic, strong) PopOverListItem *defaultUniversityPopOverItem;
@property (nonatomic, strong) PopOverListItem *defaultGraduationYearPopOverItem;
@property (nonatomic, strong) FeedbackMessageOverlay *messageOverlay;
@property (nonatomic, strong) IBOutlet UILabel *eventNameLabel;
@property (nonatomic, strong) IBOutlet UILabel *eventStatsLabel;
@property (nonatomic, strong) PasswordViewController *passwordController;
@property (strong) id <NSObject, DataEntryViewDelegate> delegate;

-(IBAction)toggleUniversityPopOverController;
-(IBAction)toggleCoursePopOverController;
-(IBAction)toggleGraduationYearPopOverController;
-(IBAction)registerButtonClicked:(id)sender;
-(IBAction)closeButtonClicked:(id)sender;
-(IBAction)textViewDidBeginEditing:(UITextView *)textView;
-(IBAction)textViewDidEndEditing:(UITextView *)textView;
-(void)resetFields;
-(void)didSelectCancel;
-(void)didValidatePassword;
-(void)setStatsLabel;
@end
