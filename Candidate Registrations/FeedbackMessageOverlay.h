//
//  FeedbackMessageOverlay.h
//  Candidate Registrations
//
//  Created by Robert Taylor on 03/08/2011.
//  Copyright 2011 Home. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FeedbackMessageOverlay : UIViewController
{
    UIButton *closeButton;
    UILabel *titleLabel;
    UITextView *contentTextView;
    UIImageView *imageView;
    BOOL showCloseButton;
    int autoCloseTimeout;
    NSString *title;
    NSString *message;
    UIView *overlayView;
    NSTimer *timer;
    UIView *containerView;
}

@property (nonatomic, strong) IBOutlet UIButton *closeButton;
@property (nonatomic, strong) IBOutlet UILabel *titleLabel;
@property (nonatomic, strong) IBOutlet UITextView *contentTextView;
@property (nonatomic, strong) IBOutlet UIImageView *imageView;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *message;
@property (nonatomic, assign) BOOL showCloseButton;
@property (nonatomic, assign) int autoCloseTimeout;
@property (nonatomic, strong) IBOutlet UIView *overlayView;
@property (nonatomic, strong) NSTimer *timer;
@property(nonatomic,strong)UIView *containerView;

-(IBAction)closeButtonPressed:(id)sender;
-(void)timerCalled;
-(void)closeOverlay;
@end
