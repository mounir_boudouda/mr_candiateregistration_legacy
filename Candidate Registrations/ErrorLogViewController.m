//
//  ErrorLogViewController.m
//  Candidate Registrations
//
//  Created by Robert Taylor on 13/08/2011.
//  Copyright 2011 Home. All rights reserved.
//

#import "ErrorLogViewController.h"

@implementation ErrorLogViewController
@synthesize tableContainerView;
@synthesize tableListController;
@synthesize clearListButton;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}


- (void)viewWillAppear:(BOOL)animated
{
    [tableListController viewWillAppear:animated];
    [super viewWillAppear:animated];
}

-(IBAction)clearButtonClicked:(id)sender
{
    DataUtils *dataUtils = [[DataUtils alloc] init];
    BOOL success = [dataUtils clearErrorList];
    if (success)
    {
        [tableListController viewWillAppear:TRUE];
    }
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    tableListController = [[ErrorLogTableViewController alloc] 
                           initWithNibName:@"ErrorLogTableViewController" 
                           bundle:nil];
    [tableContainerView addSubview:tableListController.view];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    tableListController = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
	return NO;
}

@end
