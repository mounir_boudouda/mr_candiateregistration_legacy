//
//  Candidate_RegistrationsAppDelegate.h
//  Candidate Registrations
//
//  Created by Robert Taylor on 17/07/2011.
//  Copyright 2011 Home. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PasswordViewController.h"

@interface Candidate_RegistrationsAppDelegate : NSObject <UIApplicationDelegate, PasswordViewDelegate> {
    UIWindow *window;
    UITabBarController *rootController;
    PasswordViewController *passwordController;
}

@property (nonatomic, strong) IBOutlet UIWindow *window;
@property (nonatomic, strong) IBOutlet UITabBarController *rootController;
@property (nonatomic, strong) PasswordViewController *passwordController;

- (void)didValidatePassword;
- (void)didSelectCancel;
@end
