//
//  SettingsViewController.h
//  Candidate Registrations
//
//  Created by Robert Taylor on 17/07/2011.
//  Copyright 2011 Home. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FeedbackMessageOverlay.h"


@interface SettingsViewController : UIViewController <UITextFieldDelegate>
{
    
    UITextField *passwordOldTextField;
    UITextField *passwordNewTextField;
    UITextField *repeatPasswordTextField;
    UITextField *providerIdTextField;
    UITextField *providerPasswordTextField;
    UITextField *backgroundImageTextField;
    UITextField *uniListServiceTextField;
    UITextField *eventListServiceTextField;
    UITextField *courseListServiceTextField;
    
    UIButton *changePasswordButton;
    UIButton *changeServerIdButton;
    UIButton *syncEventsButton;
    UIButton *syncUniversitiesButton;
    UIButton *syncCoursesButton;
    UIButton *changeBackgroundButton;
    
    UITextField *changeServerTextField;
    UITextField *currentTextField;
    
    UIButton *changeServerButton;
    UILabel *currentServerLabel;
    UILabel *lblVersionNumber;
    
    FeedbackMessageOverlay *messageOverlay;
    BOOL isDownloadSuccessful;
    BOOL isDataSyncSuccessful;
}

@property (nonatomic, strong) IBOutlet UITextField *passwordOldTextField;
@property (nonatomic, strong) IBOutlet UITextField *passwordNewTextField;
@property (nonatomic, strong) IBOutlet UITextField *repeatPasswordTextField;
@property (nonatomic, strong) IBOutlet UITextField *backgroundImageTextField;
@property (nonatomic, strong) IBOutlet UITextField *providerIdTextField;
@property (nonatomic, strong) IBOutlet UITextField *providerPasswordTextField;
@property (nonatomic, strong) IBOutlet UITextField *uniListServiceTextField;
@property (nonatomic, strong) IBOutlet UITextField *eventListServiceTextField;
@property (nonatomic, strong) IBOutlet UITextField *courseListServiceTextField;
@property (nonatomic, strong) IBOutlet UITextField *changeServerTextField;
@property (nonatomic, strong) UITextField *currentTextField;

@property (nonatomic, strong) IBOutlet UIButton *changePasswordButton;
@property (nonatomic, strong) IBOutlet UIButton *changeServerIdButton;
@property (nonatomic, strong) IBOutlet UIButton *changeBackgroundButton;
@property (nonatomic, strong) IBOutlet UIButton *syncEventsButton;
@property (nonatomic, strong) IBOutlet UIButton *syncUniversitiesButton;
@property (nonatomic, strong) IBOutlet UIButton *syncCoursesButton;
@property (nonatomic, strong) IBOutlet UIButton *changeServerButton;
@property (nonatomic, strong) IBOutlet UILabel *currentServerLabel;
@property (nonatomic, strong) IBOutlet UILabel *lblVersionNumber;
@property (weak, nonatomic) IBOutlet UILabel *lblBuildNumber;
@property (nonatomic, strong) FeedbackMessageOverlay *messageOverlay;
@property (nonatomic, assign) BOOL isDownloadSuccessful;
@property (nonatomic, assign) BOOL isDataSyncSuccessful;


- (IBAction)changePasswordButtonClicked:(id)sender;
- (IBAction)changeServerIdButtonClicked:(id)sender;
- (IBAction)changeServerButtonClicked:(id)sender;
- (IBAction)syncEventsButtonClicked:(id)sender;
- (IBAction)syncUniversitiesButtonClicked:(id)sender;
- (IBAction)syncCoursesButtonClicked:(id)sender;
- (IBAction)changeBackgroundButtonClicked:(id)sender;
- (BOOL)downloadBackgroundImage;
- (void)startDataSync:(NSNumber*)dataType;
- (BOOL)webFileExists:(NSString*)imageUrl;
-(void) setupCurrentServerLabel;
- (void)textFieldDidBeginEditing:(UITextField *)textField;

@end
