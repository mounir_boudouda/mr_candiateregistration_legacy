//
//  ErrorLogTableViewController.h
//  Candidate Registrations
//
//  Created by Robert Taylor on 13/08/2011.
//  Copyright 2011 Home. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ErrorLogCellView.h"
#import "ErrorLogItem.h"
#import "DataUtils.h"

@interface ErrorLogTableViewController : UITableViewController
{
    NSMutableArray *errorList;
    ErrorLogCellView *tableCell;
}
@property (nonatomic, strong) IBOutlet ErrorLogCellView *tableCell;

@end
