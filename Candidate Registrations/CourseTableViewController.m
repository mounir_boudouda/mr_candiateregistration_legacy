//
//  UniversityTableViewController.m
//  Candidate Registrations
//
//  Created by Robert Taylor on 26/07/2011.
//  Copyright 2011 Home. All rights reserved.
//

#import "CourseTableViewController.h"
#import "DataUtils.h"
#import "EventLocalProperty.h"

@implementation CourseTableViewController
@synthesize courseListArray;
@synthesize popOverDelegate;
@synthesize localCourseListArray;
@synthesize eventId;
@synthesize searchString;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    DataUtils *dataUtils = [[DataUtils alloc] init];
    courseListArray = [dataUtils loadCourseList];
    localCourseListArray = [dataUtils loadEventLocalCourseList:eventId];
    
    
    //---used for storing the search result---
    searchResult = [[NSMutableArray alloc] init];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.tableView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:NO];
    [self searchTableView:@""];
    [self.view setNeedsLayout];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
	return NO;
}

//---performs the searching using the array of states---
- (void) searchTableView:(NSString*)searchText
{
    //---clears the search result---
    searchString = [searchText copy];
    [searchResult removeAllObjects];
    if ([searchString length] > 0)
    {
        for (PopOverListItem *item in courseListArray)
        {
            NSRange titleResultsRange = 
            [item.description rangeOfString:searchString options:NSCaseInsensitiveSearch];
            if (titleResultsRange.length > 0)
                [searchResult addObject:item];
        }
    }
    else
    {
        for (PopOverListItem *item in courseListArray)
        {
            [searchResult addObject:item];
        }
    }
    [self.tableView reloadData];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    if (searchString.length>0)
        return 1;
    else if (localCourseListArray!=nil && localCourseListArray.count>0)
        return 2;
    else
        return 1;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    if (searchString.length>0)
        return [NSString stringWithFormat:@"Matching results - %d found",searchResult.count];
    else if (section == 0 && ((localCourseListArray!=nil && localCourseListArray.count>0)))
        return @"Local courses";
    else
        return @"Courses"; 
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    if (searchString.length>0) {
        return [searchResult count];
    } else if (section == 0 && ((localCourseListArray!=nil && localCourseListArray.count>0)))
        return  [localCourseListArray count];
    else
        return [searchResult count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    if (searchString.length>0)
    {
        PopOverListItem *item = (PopOverListItem *)[searchResult objectAtIndex:indexPath.row];
        cell.textLabel.text = item.description;
    }
    else if (indexPath.section == 0 && ((localCourseListArray!=nil && localCourseListArray.count>0)))
    {
        EventLocalProperty *item = (EventLocalProperty*) [localCourseListArray objectAtIndex:indexPath.row];
        PopOverListItem *popItem =  [self findCourse:item.propertyId];
        if (popItem!=nil && popItem.rowId>0)
            cell.textLabel.text = popItem.description;
    }
    else
    {
        PopOverListItem *item = (PopOverListItem *)[searchResult objectAtIndex:indexPath.row];
        cell.textLabel.text = item.description;
    }
    return cell;
}


- (PopOverListItem*) findCourse:(int)universityId
{
    int loop = 0;
    for (loop=0; loop<courseListArray.count; loop++) {
        PopOverListItem *item = [courseListArray objectAtIndex:loop];
        if (item!=nil && item.rowId == universityId)
            return item;
    }
    return nil;
}


#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (searchString.length>0)
    {
        PopOverListItem *popItem = [searchResult objectAtIndex:indexPath.row];
        [self.popOverDelegate didCourseTap:popItem];    
    }
    else if (indexPath.section == 0 && ((localCourseListArray!=nil && localCourseListArray.count>0)))
    {
        EventLocalProperty *item = (EventLocalProperty*) [localCourseListArray objectAtIndex:indexPath.row];
        PopOverListItem *popItem =  [self findCourse:item.propertyId];
        if (popItem!=nil && popItem.rowId>0)
            [self.popOverDelegate didCourseTap:popItem];
    }
    else
        [self.popOverDelegate didCourseTap:[searchResult objectAtIndex:indexPath.row]];
    
    
    
    
}

@end
