//
//  LocalUniversitiesCellView.h
//  Candidate Registrations
//
//  Created by Robert Taylor on 14/08/2011.
//  Copyright 2011 Home. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EventLocalProperty.h"
#import "PopOverListItem.h"
#import "LocalPropertyUISwitch.h"

@interface LocalPropertyCellView : UITableViewCell
{
    PopOverListItem *listItem;
    UILabel *nameLabel;
    LocalPropertyUISwitch *onOffSwitch;
    NSMutableArray *localList;
    int eventId;
}

@property (nonatomic, strong) PopOverListItem *listItem;
@property (nonatomic, strong) IBOutlet UILabel *nameLabel;
@property (nonatomic, strong) IBOutlet LocalPropertyUISwitch *onOffSwitch;
@property (nonatomic, strong) NSMutableArray *localList;
@property (nonatomic, assign) int eventId;
- (EventLocalProperty*) findProperty: (int)propertyId;
- (void) initCell;
@end
