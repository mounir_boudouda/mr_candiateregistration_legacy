//
//  CandidateDetails.m
//  Candidate Registrations
//
//  Created by Robert Taylor on 09/08/2011.
//  Copyright 2011 Home. All rights reserved.
//

#import "CandidateDetails.h"

@implementation CandidateDetails

@synthesize rowId;
@synthesize firstName;
@synthesize lastName;
@synthesize eventId;
@synthesize universityId;
@synthesize courseId;
@synthesize uploadStatus;
@synthesize graduationYear;
@synthesize dateCreated;
@synthesize email;
@synthesize universityName;
@synthesize courseName;
@synthesize uploadDate;

- (id)init
{
    self = [super init];
    if (self) {
        // Initialization code here.
    }
    
    return self;
}


@end
