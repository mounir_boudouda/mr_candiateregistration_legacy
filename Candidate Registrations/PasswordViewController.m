//
//  PasswordViewController.m
//  Candidate Registrations
//
//  Created by Robert Taylor on 24/08/2011.
//  Copyright 2011 Home. All rights reserved.
//

#import "PasswordViewController.h"
#import "DataUtils.h"

@implementation PasswordViewController
@synthesize passwordTextField;
@synthesize enterButton;
@synthesize showCancel;
@synthesize cancelButton;
@synthesize delegate;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.view.frame = [[UIScreen mainScreen] applicationFrame];
    [cancelButton setHidden:!showCancel];
    [passwordTextField becomeFirstResponder];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (IBAction) enterButtonPressed:(id)sender
{
    DataUtils *dataUtils = [[DataUtils alloc] init];
    BOOL success = [dataUtils checkPassword:passwordTextField.text];
    
    if (success)
    {
        [delegate didValidatePassword];
    }
    else
        [passwordTextField setText:@""];
}

- (IBAction) cancelButtonPressed:(id)sender
{
    [delegate didSelectCancel];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
	return NO;
}

@end
