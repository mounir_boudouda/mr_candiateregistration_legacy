//
//  EventCellView.h
//  Candidate Registrations
//
//  Created by Robert Taylor on 23/07/2011.
//  Copyright 2011 Home. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CandidateEvent.h"

@interface EventCellView : UITableViewCell
{
    UILabel *eventTitleText;
    UILabel *eventDateText;
    UILabel *uploadedLabel;
    UILabel *totalLabel;
    UILabel *notUploadedLabel;
    UIButton *updateButton;
    UIButton *dateEntryButton;
    UIButton *deleteButton;
    CandidateEvent *candidateEvent;
    int eventId;
}


@property (nonatomic, strong) IBOutlet UILabel *eventTitleText;
@property (nonatomic, strong) IBOutlet UILabel *eventDateText;
@property (nonatomic, strong) IBOutlet UILabel *uploadedLabel;
@property (nonatomic, strong) IBOutlet UILabel *notUploadedLabel;
@property (nonatomic, strong) IBOutlet UILabel *totalLabel;
@property (nonatomic, strong) IBOutlet UIButton *updateButton;
@property (nonatomic, strong) IBOutlet UIButton *dateEntryButton;
@property (nonatomic, strong) IBOutlet UIButton *deleteButton;
@property (nonatomic, strong) IBOutlet CandidateEvent *candidateEvent;
@property (nonatomic, assign) int eventId;

-(void)initEvent;
-(void)setStatsLabel;

@end