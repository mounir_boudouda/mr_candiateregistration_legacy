//
//  Constants.h
//  Candidate Registrations
//
//  Created by Robert Taylor on 06/09/2011.
//  Copyright 2011 Home. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Constants : NSObject
// Constants.h
extern NSString * const CONST_FIRSTNAME;
extern NSString * const CONST_LASTNAME;
extern NSString * const CONST_EMAILADDRESS;
//etc.
@end
