//
//  AdminEditDataViewController.m
//  Candidate Registrations
//
//  Created by Robert Taylor on 06/09/2011.
//  Copyright 2011 Home. All rights reserved.
//

#import "AdminEditDataViewController.h"

#import "DataUtils.h"
#import "QuartzCore/QuartzCore.h"
#import "CandidateEvent.h"

@implementation AdminEditDataViewController
@synthesize registerButton;
@synthesize closeButton;
@synthesize universityButton;
@synthesize courseButton;
@synthesize universityListController;
@synthesize courseListController;
@synthesize universityPopOverController;
@synthesize coursePopOverController;
@synthesize firstNameKeyboardView;
@synthesize lastNameKeyboardView;
@synthesize emailNameKeyboardView;
@synthesize candidateId;
@synthesize candidateDetails;
@synthesize defaultCoursePopOverItem;
@synthesize defaultUniversityPopOverItem;
@synthesize messageOverlay;
@synthesize defaultGraduationYearPopOverItem;
@synthesize graduationYearPopOverController;
@synthesize graduationButton;
@synthesize graduationYearListController;
@synthesize delegate;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

-(IBAction)toggleEditCandidateUniversityPopOverController {
    [self.view endEditing:YES];
    if ([universityPopOverController isPopoverVisible]) {
        
        [universityPopOverController dismissPopoverAnimated:YES];       
    } else {
        [universityPopOverController presentPopoverFromRect:CGRectMake(79, 755, 510, 36) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionDown animated:YES];
    }    
}

-(IBAction)toggleEditCandidateCoursePopOverController {
    [self.view endEditing:YES];
    if ([coursePopOverController isPopoverVisible]) {
        
        [coursePopOverController dismissPopoverAnimated:YES];
        
    } else {
        [coursePopOverController presentPopoverFromRect:CGRectMake(79, 755, 510, 36) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
        
    }    
}


-(IBAction)toggleEditCandidateGraduationYearPopOverController {
    [self.view endEditing:YES];
    if ([graduationYearPopOverController isPopoverVisible]) {
        
        [graduationYearPopOverController dismissPopoverAnimated:YES];       
    } else 
    {
        [graduationYearPopOverController presentPopoverFromRect:CGRectMake(211, 393, 177, 36) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];  
    }    
}

-(void)didUniversityTap:(PopOverListItem *)item {
    [self.view endEditing:YES];
    [universityButton setTitle:item.description forState:UIControlStateNormal];
    universityButton.tag = item.rowId;
    [universityPopOverController dismissPopoverAnimated:YES];
}

-(void)didCourseTap:(PopOverListItem *)item {
    [self.view endEditing:YES];    
    [courseButton setTitle:item.description forState:UIControlStateNormal];
    courseButton.tag = item.rowId;
    [coursePopOverController dismissPopoverAnimated:YES];    
}

-(void)didGraduationYearTap:(PopOverListItem *)item {
    [self.view endEditing:YES]; 
    if (item.rowId != 0)
    {
        NSString *concatString = @"Graduating ";
        concatString = [concatString stringByAppendingString:item.description];
        [graduationButton setTitle:concatString forState:UIControlStateNormal];
    }
    else
        [graduationButton setTitle:item.description forState:UIControlStateNormal];        
    graduationButton.tag = item.rowId;
    [graduationYearPopOverController dismissPopoverAnimated:YES];
    
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    [self.view endEditing:YES];
    [super touchesBegan:touches withEvent:event];
}


- (IBAction)editDataViewCloseButtonClicked:(id)sender
{	
    // get the current view
    UIView *currentView = self.view;
    // get the the underlying UIWindow, or the view containing the current view view
    UIView *theWindow = [currentView superview];
    
    // remove the current view and replace with myView1
    [currentView removeFromSuperview];
    
    // set up an animation for the transition between the views
    CATransition *animation = [CATransition animation];
    [animation setDuration:0.5];
    [animation setType:kCATransitionFade];
    [animation setSubtype:kCATransitionFromLeft];
    [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    
    [[theWindow layer] addAnimation:animation forKey:@"SwitchToViewTableView"];
    [delegate editDataViewDidClose];

}

- (BOOL)validateEmailWithString:(NSString*)email
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,10}"; 
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex]; 
    return [emailTest evaluateWithObject:email];
}

- (IBAction)saveEditedCandidateButtonClicked:(id)sender
{
    [self.view endEditing:YES];
    BOOL problemsFound = false;
    NSString *errorString = @"";
    NSString *trimmedString = [NSString stringWithString:firstNameKeyboardView.text];
    trimmedString = [trimmedString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if ([trimmedString caseInsensitiveCompare:@""] == NSOrderedSame || [trimmedString caseInsensitiveCompare:CONST_FIRSTNAME] == NSOrderedSame)
    {
        errorString = [errorString stringByAppendingString:@"A first name must be specified\n"];
        problemsFound = true;
    }
    
    trimmedString = [NSString stringWithString:lastNameKeyboardView.text];
    trimmedString = [trimmedString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if ([trimmedString caseInsensitiveCompare:@""] == NSOrderedSame || [trimmedString caseInsensitiveCompare:CONST_LASTNAME] == NSOrderedSame)
    {
        errorString = [errorString stringByAppendingString:@"A last name must be specified\n"];
        problemsFound = true;
    }      [self.view endEditing:YES];
    
    trimmedString = [NSString stringWithString:emailNameKeyboardView.text];
    trimmedString = [trimmedString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if ([trimmedString caseInsensitiveCompare:@""] == NSOrderedSame || [trimmedString caseInsensitiveCompare:CONST_EMAILADDRESS] == NSOrderedSame)
    {
        errorString = [errorString stringByAppendingString:@"An email address must be specified\n"];
        problemsFound = true;
    }
    if ([self validateEmailWithString:trimmedString]==false)
    {
        errorString = [errorString stringByAppendingString:@"The email address does not appear to be valid. Please check\n"];
        problemsFound = true;
    }
    
    if (problemsFound)
    {
        FeedbackMessageOverlay *messageController = [[FeedbackMessageOverlay alloc] 
                                                     initWithNibName:@"FeedbackMessageOverlay" 
                                                     bundle:nil];
        messageController.showCloseButton = TRUE;
        messageController.containerView = self.view;
        messageController.title = @"Oops!";
        messageController.message = errorString;
        self.messageOverlay = messageController;
        [self.view addSubview:messageOverlay.view];
        
    }
    else
    {
        DataUtils *dataUtils = [[DataUtils alloc] init];
        candidateDetails.firstName = firstNameKeyboardView.text;
        candidateDetails.lastName = lastNameKeyboardView.text;
        candidateDetails.email = emailNameKeyboardView.text;
        
        candidateDetails.graduationYear = graduationButton.tag;
        candidateDetails.universityId = universityButton.tag;
        candidateDetails.courseId = courseButton.tag;
        
        if ([dataUtils updateCandidateDetails:candidateDetails]==TRUE)
        {
            FeedbackMessageOverlay *messageController = [[FeedbackMessageOverlay alloc] 
                                                         initWithNibName:@"FeedbackMessageOverlay" 
                                                         bundle:nil];
            messageController.showCloseButton = TRUE;
            messageController.containerView = self.view;
            messageController.title = @"User details saved!";
            messageController.message = @"Click the back button to return";
            self.messageOverlay = messageController;
            [self.view addSubview:messageOverlay.view];
        }
        else
        {
            // try saving error to DB
            
            [dataUtils saveError:@"Couldn't save candidate details to DB"];
            
            FeedbackMessageOverlay *messageController = [[FeedbackMessageOverlay alloc] 
                                                         initWithNibName:@"FeedbackMessageOverlay" 
                                                         bundle:nil];
            messageController.showCloseButton = TRUE;
            messageController.containerView = self.view;
            messageController.title = @"Oops!";
            messageController.message = @"An error occured whist trying to save to the DB";
            self.messageOverlay = messageController;
            [self.view addSubview:messageOverlay.view];
        }
        
    }
}


- (IBAction)textViewDidBeginEditing:(UITextView *)textView
{
    if (textView.tag == 0 && [textView.text caseInsensitiveCompare:CONST_FIRSTNAME] == NSOrderedSame)// first name
        textView.text = @"";
    else     if (textView.tag == 1 && [textView.text caseInsensitiveCompare:CONST_LASTNAME] == NSOrderedSame)// last name
        textView.text = @"";
    else     if (textView.tag == 2 && [textView.text caseInsensitiveCompare:CONST_EMAILADDRESS] == NSOrderedSame)// email address
        textView.text = @"";
}

-(IBAction)textViewDidEndEditing:(UITextView *)textView
{
    if (textView.tag == 0)
    { 
        if ([textView.text caseInsensitiveCompare:@""] == NSOrderedSame)// first name
        {
            textView.text = CONST_FIRSTNAME;
        }
    }
    else if (textView.tag == 1)
    {
        if ([textView.text caseInsensitiveCompare:@""] == NSOrderedSame)// last name
        {
            textView.text = CONST_LASTNAME;
        }
    }
    else if (textView.tag == 2)
    {
        if ([textView.text caseInsensitiveCompare:@""] == NSOrderedSame)// email name
        {
            textView.text = CONST_EMAILADDRESS;
        }
    }
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.frame = [[UIScreen mainScreen] applicationFrame];
    
    DataUtils *dataUtils = [[DataUtils alloc] init];

    candidateDetails = [dataUtils loadCandidate:candidateId];
    
    if (candidateDetails == nil) return;
    
    // Do any additional setup after loading the view from its nib.
    //    self.view.frame = [[UIScreen mainScreen] applicationFrame];
    
    //[universityButton setTitle:@"Select university" forState:UIControlStateNormal];
    //[courseButton setTitle:@"Select course" forState:UIControlStateNormal];
    
    defaultCoursePopOverItem = [[PopOverListItem alloc] init];
    defaultCoursePopOverItem.rowId = 0;
    defaultCoursePopOverItem.description = @"Select Course";
    
    defaultUniversityPopOverItem = [[PopOverListItem alloc] init];
    defaultUniversityPopOverItem.rowId = 0;
    defaultUniversityPopOverItem.description = @"Select University";
    
    defaultGraduationYearPopOverItem = [[PopOverListItem alloc] init];
    defaultGraduationYearPopOverItem.rowId = 0;
    defaultGraduationYearPopOverItem.description = @"Graduation Year";
    
    [self didCourseTap:defaultCoursePopOverItem];
    [self didUniversityTap:defaultUniversityPopOverItem];
    [self didGraduationYearTap:defaultGraduationYearPopOverItem];
    
    universityListController = [[UniversityTableViewController alloc] init];
    universityListController.popOverDelegate = self;
    universityListController.eventId = candidateDetails.eventId;
    
    courseListController = [[CourseTableViewController alloc] init];
    courseListController.popOverDelegate = self;
    
    graduationYearListController = [[GraduationYearTableViewController alloc] init];
    graduationYearListController.popOverDelegate = self;
    
    universityPopOverController = [[UIPopoverController alloc] initWithContentViewController:universityListController];    
    coursePopOverController = [[UIPopoverController alloc] initWithContentViewController:courseListController];
    graduationYearPopOverController = [[UIPopoverController alloc] initWithContentViewController:graduationYearListController];
    
    universityPopOverController.popoverContentSize = CGSizeMake(768, 300);
    coursePopOverController.popoverContentSize = CGSizeMake(768, 300);
    graduationYearPopOverController.popoverContentSize = CGSizeMake(100, 300);
    
    firstNameKeyboardView.delegate = self;
    lastNameKeyboardView.delegate = self;
    emailNameKeyboardView.delegate = self;
    
    [firstNameKeyboardView setText:candidateDetails.firstName];
    [lastNameKeyboardView setText:candidateDetails.lastName];
    [emailNameKeyboardView setText:candidateDetails.email];
    
    if (candidateDetails.universityId>0)
    {
        [universityButton setTitle:candidateDetails.universityName forState:UIControlStateNormal];
        [universityButton setTag:candidateDetails.universityId];
    }
    if (candidateDetails.courseId>0)
    {
        [courseButton setTitle:candidateDetails.courseName forState:UIControlStateNormal];
        [courseButton setTag:candidateDetails.courseId];
    }
    if (candidateDetails.graduationYear>0)
    {
        [graduationButton setTitle:[NSString stringWithFormat:@"Graduating %d",candidateDetails.graduationYear] forState:UIControlStateNormal];
        [graduationButton setTag:candidateDetails.graduationYear];
    }
    
}



- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
	return NO;
}


@end