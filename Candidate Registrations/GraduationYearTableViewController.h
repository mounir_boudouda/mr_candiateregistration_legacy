//
//  GraduationYearTableViewController.h
//  Candidate Registrations
//
//  Created by Robert Taylor on 05/08/2011.
//  Copyright 2011 Home. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PopOverListItem.h"

@protocol GraduationYearPopOverControllerDelegate <NSObject>

-(void)didGraduationYearTap:(PopOverListItem*)item;

@end

@interface GraduationYearTableViewController : UITableViewController
{
    NSMutableArray *gradYearListArray;
    __weak id  popOverDelegate;
}

@property (nonatomic, weak) id<GraduationYearPopOverControllerDelegate> popOverDelegate;
@property (nonatomic, strong) NSMutableArray *gradYearListArray;

- (void)populateGraduationYearList;
@end
