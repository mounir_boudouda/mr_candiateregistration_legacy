//
//  CandidateTableViewController.m
//  Candidate Registrations
//
//  Created by Robert Taylor on 10/08/2011.
//  Copyright 2011 Home. All rights reserved.
//

#import "CandidateTableViewController.h"
#import "DataUtils.h"
#import "Candidate_RegistrationsAppDelegate.h"
#import "QuartzCore/QuartzCore.h"

@implementation CandidateTableViewController
@synthesize eventId;
@synthesize candidateTableCell;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}


#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Uncomment the following line to preserve selection between presentations.
    self.clearsSelectionOnViewWillAppear = YES;
    
    self.tableView.rowHeight = 72;
//    self.tableView.backgroundColor = [UIColor whiteColor];
    [self viewWillAppear:true];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    DataUtils *dataUtils = [[DataUtils alloc] init];
    
    candidatesList = [dataUtils loadCandidateList:eventId];
    
    
    [self.tableView reloadData];
    
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
	return NO;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (candidatesList != nil) {
        return [candidatesList count];
    }
    else
    {
        return 0;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    CandidateCellView *cell = (CandidateCellView *) [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if(cell == nil) 
    {
        [[NSBundle mainBundle] loadNibNamed:@"CandidateCellView" owner:self options:nil];
        cell = candidateTableCell;
                
        // Configure the cell...
        CandidateDetails *detail = (CandidateDetails *)[candidatesList objectAtIndex:indexPath.row];
        cell.candidateDetails = detail;
        [cell initCandidate];
    }
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

}

-(IBAction)editButtonPressed:(id)sender
{
    // don't allow edit if already uploaded
    if  (((UIButton*)sender).tag ==0) return;
    
     adminEditDataViewController = [[AdminEditDataViewController alloc] initWithNibName:@"AdminEditDataViewController" bundle:nil];
    
    adminEditDataViewController.candidateId = ((UIButton*)sender).tag;
    adminEditDataViewController.delegate = self;
    
    Candidate_RegistrationsAppDelegate *appDelegate = (Candidate_RegistrationsAppDelegate *)[[UIApplication sharedApplication] delegate];
    
    [appDelegate.window addSubview:adminEditDataViewController.view];   
    
    // set up an animation for the transition between the views
	CATransition *animation = [CATransition animation];
	[animation setDuration:0.5];
	[animation setType:kCATransitionFade];
	[animation setSubtype:kCATransitionFromLeft];
	[animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
	
	[[appDelegate.window layer] addAnimation:animation forKey:@"SwitchToView1"];
}

-(void)editDataViewDidClose
{
    [self viewWillAppear:false];
}
@end
