//
//  WebserviceUtils.h
//  Candidate Registrations
//
//  Created by Robert Taylor on 14/08/2011.
//  Copyright 2011 Home. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SBJson.h"
#import "PopOverListItem.h"
#import "CandidateEvent.h"
#import "CandidateDetails.h"


@interface WebserviceUtils : NSObject
{

}

- (NSMutableArray*)downloadUniversityList:(NSString*)serviceUrl:(NSString*)serverId:(NSString*)serverPassword;
- (NSMutableArray*)downloadCourseList:(NSString*)serviceUrl:(NSString*)serverId:(NSString*)serverPassword;
- (NSMutableArray*)downloadEventList:(NSString*)serviceUrl:(NSString*)serverId:(NSString*)serverPassword;
- (NSString *)stringWithUrl:(NSURL *)url;
- (id) objectWithUrl:(NSURL *)url;
- (int)registerCandidate:(NSString*)serverName:(CandidateDetails*)candidate:(NSString*)serverId:(NSString*)serverPassword;
- (NSDate*)dateFromDotNetJSONString:(NSString *)string;
- (NSString *)cleanListDescription:(NSString *)searchTerm;
@end
