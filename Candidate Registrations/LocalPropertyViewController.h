//
//  LocalUniversitiesViewController.h
//  Candidate Registrations
//
//  Created by Robert Taylor on 14/08/2011.
//  Copyright 2011 Home. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LocalPropertyTableViewController.h"

@interface LocalPropertyViewController : UIViewController
{
    LocalPropertyTableViewController *tableViewController;
    UIView *tableView;
    UIButton *backButton;
    int eventId;
    bool isUniversityList;
}

@property (nonatomic, strong) IBOutlet LocalPropertyTableViewController *tableViewController;
@property (nonatomic, strong) IBOutlet UIView *tableView; 
@property (nonatomic, strong) IBOutlet UIButton *backButton;
@property (nonatomic, assign) int eventId;
@property (nonatomic, assign) bool isUniversityList;
@end
