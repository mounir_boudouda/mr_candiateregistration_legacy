//
//  CandidateEvent.m
//  Candidate Registrations
//
//  Created by Robert Taylor on 17/07/2011.
//  Copyright 2011 Home. All rights reserved.
//

#import "CandidateEvent.h"


@implementation CandidateEvent
@synthesize eventDate,eventName,eventId,rowId;

-(void) setEventName:(NSString *)input
{
    eventName = input; 
}

-(void) setEventDate:(NSDate *)input
{
    eventDate = input;
}



@end
