//
//  LocalUniversitiesTableViewController.m
//  Candidate Registrations
//
//  Created by Robert Taylor on 14/08/2011.
//  Copyright 2011 Home. All rights reserved.
//

#import "LocalPropertyTableViewController.h"
#import "DataUtils.h"
#import "PopOverListItem.h"
#import "LocalPropertyUISwitch.h"

@implementation LocalPropertyTableViewController
@synthesize eventId;
@synthesize propertyTableCell;
@synthesize isUniversityList;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}



#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];

    DataUtils *dataUtils = [[DataUtils alloc] init];
    
    if (isUniversityList)
    {
        globalPropertyList = [dataUtils loadUniversityList];
        localPropertyList = [dataUtils loadEventLocalUniversityList:eventId];
    }
    else
    {
        globalPropertyList = [dataUtils loadCourseList];
        localPropertyList = [dataUtils loadEventLocalCourseList:eventId];       
    }
    
    // Uncomment the following line to preserve selection between presentations.
    self.clearsSelectionOnViewWillAppear = NO;
    
    self.tableView.rowHeight = 44;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
	return NO;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    if (globalPropertyList!=nil)
        return globalPropertyList.count;
    else
        return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    LocalPropertyCellView *cell = (LocalPropertyCellView *) [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if(cell == nil) 
    {
        [[NSBundle mainBundle] loadNibNamed:@"LocalPropertyCellView" owner:self options:nil];
        cell = propertyTableCell;
        
        // Configure the cell...
        PopOverListItem *detail = (PopOverListItem *)[globalPropertyList objectAtIndex:indexPath.row];
        cell.listItem = detail;
        cell.eventId = eventId;
        cell.localList = localPropertyList;
        [cell initCell];
    }
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     [detailViewController release];
     */
}


-(IBAction)propertySwitchSelected:(id)sender
{
    DataUtils *utils = [[DataUtils alloc] init];
    
    LocalPropertyUISwitch *toggleSwitch = (LocalPropertyUISwitch*)sender;
    int localEventId = toggleSwitch.EventId;
    int propertyId = toggleSwitch.PropertyId;

    if (isUniversityList)
    {
        if (toggleSwitch.on)
        {
            [utils insertEventLocalUniversity:localEventId :propertyId :0];
        }
        else
        {
            [utils removeEventLocalUniversity:localEventId :propertyId];
        }
    }
    else
    {
        if (toggleSwitch.on)
        {
            [utils insertEventLocalCourse:localEventId :propertyId :0];
        }
        else
        {
            [utils removeEventLocalCourse:localEventId :propertyId];
        }        
    }
    
}

@end
