//
//  ErrorLogItem.m
//  Candidate Registrations
//
//  Created by Robert Taylor on 13/08/2011.
//  Copyright 2011 Home. All rights reserved.
//

#import "ErrorLogItem.h"

@implementation ErrorLogItem

@synthesize rowId;
@synthesize errorDescription;
@synthesize errorDate;

- (id)init
{
    self = [super init];
    if (self) {
        // Initialization code here.
    }
    
    return self;
}
@end
