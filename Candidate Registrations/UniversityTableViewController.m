//
//  UniversityTableViewController.m
//  Candidate Registrations
//
//  Created by Robert Taylor on 26/07/2011.
//  Copyright 2011 Home. All rights reserved.
//

#import "UniversityTableViewController.h"
#import "DataUtils.h"
#import "EventLocalProperty.h"

@implementation UniversityTableViewController
@synthesize universityListArray;
@synthesize popOverDelegate;
@synthesize localUniversityListArray;
@synthesize eventId;
@synthesize searchString;

//@synthesize searchBar;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    DataUtils *dataUtils = [[DataUtils alloc] init];
    universityListArray = [dataUtils loadUniversityList];
    localUniversityListArray = [dataUtils loadEventLocalUniversityList:eventId];

    //---used for storing the search result---
    searchResult = [[NSMutableArray alloc] init];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.tableView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:NO];
    [self searchTableView:@""];
    [self.view setNeedsLayout];
//    [searchBar becomeFirstResponder];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
	return NO;
}

//---performs the searching using the array of states---
- (void) searchTableView:(NSString*)searchText
{
    //---clears the search result---
    searchString = [searchText copy];
    
    [searchResult removeAllObjects];
    if ([searchString length] > 0)
    {
        for (PopOverListItem *item in universityListArray)
        {
            NSRange titleResultsRange = 
            [item.description rangeOfString:searchString options:NSCaseInsensitiveSearch];
            if (titleResultsRange.length > 0)
                [searchResult addObject:item];
        }
    }
    else
    {
        for (PopOverListItem *item in universityListArray)
        {
            [searchResult addObject:item];
        }
    }
    [self.tableView reloadData];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    if (searchString.length>0)
        return 1;
    else if (localUniversityListArray!=nil && localUniversityListArray.count>0)
        return 2;
    else
        return 1;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    if (searchString.length>0)
        return [NSString stringWithFormat:@"Matching results - %d found",searchResult.count];
    else if (section == 0 && ((localUniversityListArray!=nil && localUniversityListArray.count>0)))
        return @"Local universities & colleges";
    else
        return @"Universities & colleges"; 
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    // Return the number of rows in the section.
    if (searchString.length>0) {
        return [searchResult count];
    } else if (section == 0 && ((localUniversityListArray!=nil && localUniversityListArray.count>0)))
        return  [localUniversityListArray count];
    else
        return [searchResult count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    if (searchString.length>0)
    {
        PopOverListItem *item = (PopOverListItem *)[searchResult objectAtIndex:indexPath.row];
        cell.textLabel.text = item.description;
    }
    else if (indexPath.section == 0 && ((localUniversityListArray!=nil && localUniversityListArray.count>0)))
    {
        EventLocalProperty *item = (EventLocalProperty*) [localUniversityListArray objectAtIndex:indexPath.row];
        PopOverListItem *popItem =  [self findUniversity:item.propertyId];
        if (popItem!=nil && popItem.rowId>0)
            cell.textLabel.text = popItem.description;
    }
    else
    {
        PopOverListItem *item = (PopOverListItem *)[searchResult objectAtIndex:indexPath.row];
        cell.textLabel.text = item.description;
    }
    return cell;
}

- (PopOverListItem*) findUniversity:(int)universityId
{
    int loop = 0;
    for (loop=0; loop<universityListArray.count; loop++) {
        PopOverListItem *item = [universityListArray objectAtIndex:loop];
        if (item!=nil && item.rowId == universityId)
            return item;
    }
    return nil;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (searchString.length>0)
    {
        PopOverListItem *popItem = [searchResult objectAtIndex:indexPath.row];
        [self.popOverDelegate didUniversityTap:popItem];    
    }
    else if (indexPath.section == 0 && ((localUniversityListArray!=nil && localUniversityListArray.count>0)))
    {
        EventLocalProperty *item = (EventLocalProperty*) [localUniversityListArray objectAtIndex:indexPath.row];
        PopOverListItem *popItem =  [self findUniversity:item.propertyId];
        if (popItem!=nil && popItem.rowId>0)
        [self.popOverDelegate didUniversityTap:popItem];
    }
    else
        [self.popOverDelegate didUniversityTap:[searchResult objectAtIndex:indexPath.row]];
}

@end
