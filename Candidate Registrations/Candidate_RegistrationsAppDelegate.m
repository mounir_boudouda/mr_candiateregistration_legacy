//
//  Candidate_RegistrationsAppDelegate.m
//  Candidate Registrations
//
//  Created by Robert Taylor on 17/07/2011.
//  Copyright 2011 Home. All rights reserved.
//

#import "Candidate_RegistrationsAppDelegate.h"

@implementation Candidate_RegistrationsAppDelegate

@synthesize window;
@synthesize passwordController;
@synthesize rootController;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [self.window addSubview:rootController.view];
    
    passwordController = [[PasswordViewController alloc] 
                                                 initWithNibName:@"PasswordViewController" 
                                                 bundle:nil];
    passwordController.delegate = self;
    passwordController.showCancel = false;
    [self.window addSubview:passwordController.view];
    [self.window makeKeyAndVisible];
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    /*
     Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
     Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
     */
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    /*
     Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
     If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
     */
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    /*
     Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
     */
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    /*
     Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
     */
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    /*
     Called when the application is about to terminate.
     Save data if appropriate.
     See also applicationDidEnterBackground:.
     */
}

- (void)didValidatePassword
{
    [passwordController.view removeFromSuperview];
}

-(void)didSelectCancel
{
    // will never be hit as we don't display cancel button
}


@end
