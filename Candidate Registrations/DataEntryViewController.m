//
//  DataEntryViewController.m
//  Candidate Registrations
//
//  Created by Robert Taylor on 23/07/2011.
//  Copyright 2011 Home. All rights reserved.
//

#import "DataEntryViewController.h"
#import "DataUtils.h"
#import "QuartzCore/QuartzCore.h"
#import "CandidateEvent.h"

@implementation DataEntryViewController
@synthesize registerButton;
@synthesize closeButton;
@synthesize universityButton;
@synthesize courseButton;
@synthesize universityListController;
@synthesize courseListController;
@synthesize universityPopOverController;
@synthesize coursePopOverController;
@synthesize firstNameKeyboardView;
@synthesize lastNameKeyboardView;
@synthesize emailNameKeyboardView;
@synthesize eventId;
@synthesize defaultCoursePopOverItem;
@synthesize defaultUniversityPopOverItem;
@synthesize messageOverlay;
@synthesize defaultGraduationYearPopOverItem;
@synthesize graduationYearPopOverController;
@synthesize graduationButton;
@synthesize graduationYearListController;
@synthesize eventNameLabel;
@synthesize backgroundImageView;
@synthesize passwordController;
@synthesize eventStatsLabel;
@synthesize delegate;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

-(IBAction)toggleUniversityPopOverController {
    [self.view endEditing:YES];
    if ([universityPopOverController isPopoverVisible]) {
        
        [universityPopOverController dismissPopoverAnimated:YES];       
    } else {
        [universityPopOverController presentPopoverFromRect:CGRectMake(79, 755, 510, 36) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionDown animated:YES];
    }    
}

-(IBAction)toggleCoursePopOverController {
    [self.view endEditing:YES];
    if ([coursePopOverController isPopoverVisible]) {
        
        [coursePopOverController dismissPopoverAnimated:YES];
        
    } else {
        [coursePopOverController presentPopoverFromRect:CGRectMake(79, 755, 510, 36) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionDown animated:YES];
        
    }    
}


-(IBAction)toggleGraduationYearPopOverController {
    [self.view endEditing:YES];
    if ([graduationYearPopOverController isPopoverVisible]) {
        
        [graduationYearPopOverController dismissPopoverAnimated:YES];       
    } else 
    {
        [graduationYearPopOverController presentPopoverFromRect:CGRectMake(411, 643, 177, 36) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];  
    }    
}

-(void)didUniversityTap:(PopOverListItem *)item {
    [self.view endEditing:YES];
    [universityButton setTitle:item.description forState:UIControlStateNormal];
    universityButton.tag = item.rowId;
    [universityPopOverController dismissPopoverAnimated:YES];
    if (item.rowId>0) {
        [universityButton setBackgroundColor:[UIColor colorWithRed:187.0f/255.0f green:240.0f/255.0f blue:196.0f/255.0f alpha:1.0f]];
        [universityButton setTitleColor:[UIColor colorWithRed:0.0f/255.0f green:0.0f/255.0f blue:0.0f/255.0f alpha:1.0f] forState:UIControlStateNormal];
    }
    else
    {
        [universityButton setBackgroundColor:[UIColor colorWithRed:244.0f/255.0f green:212.0f/255.0f blue:212.0f/255.0f alpha:1.0f]];
        [universityButton setTitleColor:[UIColor colorWithRed:150.0f/255.0f green:150.0f/255.0f blue:150.0f/255.0f alpha:1.0f] forState:UIControlStateNormal];
    }
}

-(void)didCourseTap:(PopOverListItem *)item {
    [self.view endEditing:YES];    
    [courseButton setTitle:item.description forState:UIControlStateNormal];
    courseButton.tag = item.rowId;
    [coursePopOverController dismissPopoverAnimated:YES];
    if (item.rowId>0) {
        [courseButton setBackgroundColor:[UIColor colorWithRed:187.0f/255.0f green:240.0f/255.0f blue:196.0f/255.0f alpha:1.0f]];
        [courseButton setTitleColor:[UIColor colorWithRed:0.0f/255.0f green:0.0f/255.0f blue:0.0f/255.0f alpha:1.0f] forState:UIControlStateNormal];
    }
    else
    {
        [courseButton setBackgroundColor:[UIColor colorWithRed:244.0f/255.0f green:212.0f/255.0f blue:212.0f/255.0f alpha:1.0f]];
        [courseButton setTitleColor:[UIColor colorWithRed:150.0f/255.0f green:150.0f/255.0f blue:150.0f/255.0f alpha:1.0f] forState:UIControlStateNormal];
   }
    
}

-(void)didGraduationYearTap:(PopOverListItem *)item {
    [self.view endEditing:YES]; 
    if (item.rowId != 0)
    {
        NSString *concatString = @"Graduating ";
        concatString = [concatString stringByAppendingString:item.description];
        [graduationButton setTitle:concatString forState:UIControlStateNormal];
    }
    else
        [graduationButton setTitle:item.description forState:UIControlStateNormal];        
    graduationButton.tag = item.rowId;
    [graduationYearPopOverController dismissPopoverAnimated:YES];
    if (item.rowId>0) {
        [graduationButton setBackgroundColor:[UIColor colorWithRed:187.0f/255.0f green:240.0f/255.0f blue:196.0f/255.0f alpha:1.0f]];
        [graduationButton setTitleColor:[UIColor colorWithRed:0.0f/255.0f green:0.0f/255.0f blue:0.0f/255.0f alpha:1.0f] forState:UIControlStateNormal];
    }
    else
    {
        [graduationButton setBackgroundColor:[UIColor colorWithRed:244.0f/255.0f green:212.0f/255.0f blue:212.0f/255.0f alpha:1.0f]];
        [graduationButton setTitleColor:[UIColor colorWithRed:150.0f/255.0f green:150.0f/255.0f blue:150.0f/255.0f alpha:1.0f] forState:UIControlStateNormal];
    }
 
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    [self.view endEditing:YES];
    [super touchesBegan:touches withEvent:event];
}

- (void)didSelectCancel
{
    [passwordController.view removeFromSuperview];
}

- (void)didValidatePassword
{
    // get the current view
    UIView *currentView = self.view;
    // get the the underlying UIWindow, or the view containing the current view view
    UIView *theWindow = [currentView superview];
    
    // remove the current view and replace with myView1
    [currentView removeFromSuperview];
    
    // set up an animation for the transition between the views
    CATransition *animation = [CATransition animation];
    [animation setDuration:0.5];
    [animation setType:kCATransitionFade];
    [animation setSubtype:kCATransitionFromLeft];
    [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    
    [[theWindow layer] addAnimation:animation forKey:@"SwitchToViewTableView"];
    [delegate dataEntryViewDidClose];
}


- (IBAction)closeButtonClicked:(id)sender
{	
    // first show password screen and validate
    
    passwordController = [[PasswordViewController alloc] 
                          initWithNibName:@"PasswordViewController" 
                          bundle:nil];
    passwordController.showCancel = true;
    passwordController.delegate = self;
    [self.view addSubview:passwordController.view];
}

- (BOOL)validateEmailWithString:(NSString*)email
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,10}"; 
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex]; 
    return [emailTest evaluateWithObject:email];
}

- (IBAction)registerButtonClicked:(id)sender
{
    [self.view endEditing:YES];
    BOOL problemsFound = false;
    NSString *errorString = @"";
    NSString *firstname = [NSString stringWithString:firstNameKeyboardView.text];
    firstname = [firstname stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if ([firstname caseInsensitiveCompare:@""] == NSOrderedSame || [firstname caseInsensitiveCompare:CONST_FIRSTNAME] == NSOrderedSame)
    {
        errorString = [errorString stringByAppendingString:@"A first name must be specified\n"];
        problemsFound = true;
    }
    
    NSString *lastname = [NSString stringWithString:lastNameKeyboardView.text];
    lastname = [lastname stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if ([lastname caseInsensitiveCompare:@""] == NSOrderedSame || [lastname caseInsensitiveCompare:CONST_LASTNAME] == NSOrderedSame)
    {
        errorString = [errorString stringByAppendingString:@"A last name must be specified\n"];
        problemsFound = true;
    }      [self.view endEditing:YES];

    NSString *email = [NSString stringWithString:emailNameKeyboardView.text];
    email = [email stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if ([email caseInsensitiveCompare:@""] == NSOrderedSame || [email caseInsensitiveCompare:CONST_EMAILADDRESS] == NSOrderedSame)
    {
        errorString = [errorString stringByAppendingString:@"An email address must be specified\n"];
        problemsFound = true;
    }
    if ([self validateEmailWithString:email]==false)
    {
        errorString = [errorString stringByAppendingString:@"The email address does not appear to be valid. Please check\n"];
        problemsFound = true;
    }
    
    NSString *course = [NSString stringWithString:courseButton.currentTitle];
    course = [course stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if ([course isEqualToString:@"Select Course Area"])
    {
        errorString = [errorString stringByAppendingString:@"A course must be specified\n"];
        problemsFound = true;
    }
    
    NSString *university = [NSString stringWithString:universityButton.currentTitle];
    university = [university stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if ([university isEqualToString:@"Select University"])
    {
        errorString = [errorString stringByAppendingString:@"A university must be specified\n"];
        problemsFound = true;
    }
    
    NSString *graduationYear = [NSString stringWithString:graduationButton.currentTitle];
    graduationYear = [graduationYear stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if ([graduationYear isEqualToString:@"Graduation Year"])
    {
        errorString = [errorString stringByAppendingString:@"A graduation year must be specified\n"];
        problemsFound = true;
    }
    
    
    
//    NSString *universityName = [NSString stringWithString:universityButton.currentTitle];
//    universityName = [universityName stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
//    if ([universityName caseInsensitiveCompare:@""] == NSOrderedSame || [universityName caseInsensitiveCompare:] == NSOrderedSame)
//    {
//        errorString = [errorString stringByAppendingString:@"An email address must be specified\n"];
//        problemsFound = true;
//    }
    
    if (problemsFound)
    {
        FeedbackMessageOverlay *messageController = [[FeedbackMessageOverlay alloc] 
                               initWithNibName:@"FeedbackMessageOverlay" 
                               bundle:nil];
        messageController.showCloseButton = TRUE;
        messageController.title = @"Oops!";
        messageController.message = errorString;
        messageController.containerView = self.view;
        self.messageOverlay = messageController;
        [self.view addSubview:messageOverlay.view];
       
    }
    else
    {
        DataUtils *dataUtils = [[DataUtils alloc] init];
        if ([dataUtils saveCandidateRegistration:eventId :universityButton.tag :courseButton.tag :firstname:lastname :email :graduationButton.tag]==TRUE)
        {
            FeedbackMessageOverlay *messageController = [[FeedbackMessageOverlay alloc] 
                                                         initWithNibName:@"FeedbackMessageOverlay" 
                                                         bundle:nil];
            messageController.showCloseButton = FALSE;
            messageController.autoCloseTimeout = 2;
            messageController.containerView = self.view;
           messageController.title = @"Welcome to Milkround!";
            messageController.message = @"Congratulations, you've been registered. We'll send you a welcome email in the next few days.";
            self.messageOverlay = messageController;
            [self.view addSubview:messageOverlay.view];
            
            
            [self didCourseTap:defaultCoursePopOverItem];
            [self didUniversityTap:defaultUniversityPopOverItem];
            [self didGraduationYearTap:defaultGraduationYearPopOverItem];
            [self resetFields];
            [self setStatsLabel];
        }
        else
        {
            // try saving error to DB
            
            [dataUtils saveError:@"Couldn't save candidate details to DB"];
            
            FeedbackMessageOverlay *messageController = [[FeedbackMessageOverlay alloc] 
                                                         initWithNibName:@"FeedbackMessageOverlay" 
                                                         bundle:nil];
            messageController.showCloseButton = TRUE;
            messageController.title = @"Oops!";
            messageController.containerView = self.view;
            messageController.message = @"An error occured whist trying to save to the DB";
            self.messageOverlay = messageController;
            [self.view addSubview:messageOverlay.view];
        }
    }
}



- (IBAction)textViewDidBeginEditing:(UITextView *)textView
{
    if (textView.tag == 0 && [textView.text caseInsensitiveCompare:CONST_FIRSTNAME] == NSOrderedSame)// first name
        textView.text = @"";
    else     if (textView.tag == 1 && [textView.text caseInsensitiveCompare:CONST_LASTNAME] == NSOrderedSame)// last name
        textView.text = @"";
    else     if (textView.tag == 2 && [textView.text caseInsensitiveCompare:CONST_EMAILADDRESS] == NSOrderedSame)// email address
        textView.text = @"";
    [textView setBackgroundColor:[UIColor colorWithRed:219.0f/255.0f green:240.0f/255.0f blue:246.0f/255.0f alpha:1.0f]];
    [textView setTextColor:[UIColor colorWithRed:0.0f/255.0f green:0.0f/255.0f blue:0.0f/255.0f alpha:1.0f]];
}

-(IBAction)textViewDidEndEditing:(UITextView *)textView
{
    if (textView.tag == 0)
    { 
        if ([textView.text caseInsensitiveCompare:@""] == NSOrderedSame)// first name
        {
            textView.text = CONST_FIRSTNAME;
            [firstNameKeyboardView setBackgroundColor:[UIColor colorWithRed:244.0f/255.0f green:212.0f/255.0f blue:212.0f/255.0f alpha:1.0f]];
            [firstNameKeyboardView setTextColor:[UIColor colorWithRed:150.0f/255.0f green:150.0f/255.0f blue:150.0f/255.0f alpha:1.0f]];
        }
        else
        {
            [firstNameKeyboardView setBackgroundColor:[UIColor colorWithRed:187.0f/255.0f green:240.0f/255.0f blue:196.0f/255.0f alpha:1.0f]];
            [firstNameKeyboardView setTextColor:[UIColor colorWithRed:0.0f/255.0f green:0.0f/255.0f blue:0.0f/255.0f alpha:1.0f]];
        }
    }
    else if (textView.tag == 1)
    {
        if ([textView.text caseInsensitiveCompare:@""] == NSOrderedSame)// last name
        {
            textView.text = CONST_LASTNAME;
            [lastNameKeyboardView setBackgroundColor:[UIColor colorWithRed:244.0f/255.0f green:212.0f/255.0f blue:212.0f/255.0f alpha:1.0f]];
            [lastNameKeyboardView setTextColor:[UIColor colorWithRed:150.0f/255.0f green:150.0f/255.0f blue:150.0f/255.0f alpha:1.0f]];
        }
        else
        {
            [lastNameKeyboardView setBackgroundColor:[UIColor colorWithRed:187.0f/255.0f green:240.0f/255.0f blue:196.0f/255.0f alpha:1.0f]];
            [lastNameKeyboardView setTextColor:[UIColor colorWithRed:0.0f/255.0f green:0.0f/255.0f blue:0.0f/255.0f alpha:1.0f]];
        }        
    }
    else if (textView.tag == 2)
    {
        if ([textView.text caseInsensitiveCompare:@""] == NSOrderedSame)// email name
        {
            textView.text = CONST_EMAILADDRESS;
            [emailNameKeyboardView setBackgroundColor:[UIColor colorWithRed:244.0f/255.0f green:212.0f/255.0f blue:212.0f/255.0f alpha:1.0f]];
            [emailNameKeyboardView setTextColor:[UIColor colorWithRed:150.0f/255.0f green:150.0f/255.0f blue:150.0f/255.0f alpha:1.0f]];
        }
        else
        {
            [emailNameKeyboardView setBackgroundColor:[UIColor colorWithRed:187.0f/255.0f green:240.0f/255.0f blue:196.0f/255.0f alpha:1.0f]];
            [emailNameKeyboardView setTextColor:[UIColor colorWithRed:0.0f/255.0f green:0.0f/255.0f blue:0.0f/255.0f alpha:1.0f]];
        }        
    }
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
//    self.view.frame = [[UIScreen mainScreen] applicationFrame];
    
    //[universityButton setTitle:@"Select university" forState:UIControlStateNormal];
    //[courseButton setTitle:@"Select course" forState:UIControlStateNormal];
    
    defaultCoursePopOverItem = [[PopOverListItem alloc] init];
    defaultCoursePopOverItem.rowId = 0;
    defaultCoursePopOverItem.description = @"Select Course Area";
    
    defaultUniversityPopOverItem = [[PopOverListItem alloc] init];
    defaultUniversityPopOverItem.rowId = 0;
    defaultUniversityPopOverItem.description = @"Select University";
    
    defaultGraduationYearPopOverItem = [[PopOverListItem alloc] init];
    defaultGraduationYearPopOverItem.rowId = 0;
    defaultGraduationYearPopOverItem.description = @"Graduation Year";
    
    [self didCourseTap:defaultCoursePopOverItem];
    [self didUniversityTap:defaultUniversityPopOverItem];
    [self didGraduationYearTap:defaultGraduationYearPopOverItem];
    
    universityListController = [[SelectUniversityListViewController alloc] init];
    universityListController.popOverDelegate = self;
    universityListController.eventId = eventId;

    courseListController = [[SelectCourseListViewController alloc] init];
    courseListController.popOverDelegate = self;
    courseListController.eventId = eventId;
    
    graduationYearListController = [[GraduationYearTableViewController alloc] init];
    graduationYearListController.popOverDelegate = self;

    universityPopOverController = [[UIPopoverController alloc] initWithContentViewController:universityListController];    
    coursePopOverController = [[UIPopoverController alloc] initWithContentViewController:courseListController];
    graduationYearPopOverController = [[UIPopoverController alloc] initWithContentViewController:graduationYearListController];
    
    universityPopOverController.popoverContentSize = CGSizeMake(768, 300);
    coursePopOverController.popoverContentSize = CGSizeMake(768, 300);
    graduationYearPopOverController.popoverContentSize = CGSizeMake(100, 300);
    
    firstNameKeyboardView.delegate = self;
    lastNameKeyboardView.delegate = self;
    emailNameKeyboardView.delegate = self;
    
    //get the background image
	NSString *docDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
	NSString *pngFilePath = [NSString stringWithFormat:@"%@/background.png",docDir];
    NSFileManager *filemgr;
    filemgr = [NSFileManager defaultManager];
    if ([filemgr fileExistsAtPath: pngFilePath ] == YES)
        backgroundImageView.image = [[UIImage alloc] initWithContentsOfFile:pngFilePath];
    
    //Set the event title
    DataUtils *dataUtils = [[DataUtils alloc] init];
    CandidateEvent *event = [dataUtils loadEventByEventId:eventId];
    [eventNameLabel setText: event.eventName];
    [self resetFields];
    
    [self setStatsLabel];
}

- (void)setStatsLabel
{
    DataUtils *dataUtils = [[DataUtils alloc] init];
    int uploadedCount = [dataUtils getCandidateCounts:true :eventId];
    int notUploadedCount = [dataUtils getCandidateCounts:false:eventId];
    
    [eventStatsLabel setText:[NSString stringWithFormat:@"Stats - T:%d    U:%d    N:%d",uploadedCount+notUploadedCount,uploadedCount,notUploadedCount]];
    
}

- (void)resetFields
{
    [firstNameKeyboardView setText:CONST_FIRSTNAME];
    [firstNameKeyboardView setBackgroundColor:[UIColor colorWithRed:244.0f/255.0f green:212.0f/255.0f blue:212.0f/255.0f alpha:1.0f]];
    [firstNameKeyboardView setTextColor:[UIColor colorWithRed:150.0f/255.0f green:150.0f/255.0f blue:150.0f/255.0f alpha:1.0f]];
    
    [lastNameKeyboardView setText:CONST_LASTNAME];
    [lastNameKeyboardView setBackgroundColor:[UIColor colorWithRed:244.0f/255.0f green:212.0f/255.0f blue:212.0f/255.0f alpha:1.0f]];
    [lastNameKeyboardView setTextColor:[UIColor colorWithRed:150.0f/255.0f green:150.0f/255.0f blue:150.0f/255.0f alpha:1.0f]];

    
    [emailNameKeyboardView setText:CONST_EMAILADDRESS];
    [emailNameKeyboardView setBackgroundColor:[UIColor colorWithRed:244.0f/255.0f green:212.0f/255.0f blue:212.0f/255.0f alpha:1.0f]];
    [emailNameKeyboardView setTextColor:[UIColor colorWithRed:150.0f/255.0f green:150.0f/255.0f blue:150.0f/255.0f alpha:1.0f]];


    [universityButton setBackgroundColor:[UIColor colorWithRed:244.0f/255.0f green:212.0f/255.0f blue:212.0f/255.0f alpha:1.0f]];
    [universityButton setTitleColor:[UIColor colorWithRed:150.0f/255.0f green:150.0f/255.0f blue:150.0f/255.0f alpha:1.0f] forState:UIControlStateNormal];
    [courseButton setBackgroundColor:[UIColor colorWithRed:244.0f/255.0f green:212.0f/255.0f blue:212.0f/255.0f alpha:1.0f]];
    [courseButton setTitleColor:[UIColor colorWithRed:150.0f/255.0f green:150.0f/255.0f blue:150.0f/255.0f alpha:1.0f] forState:UIControlStateNormal];
    [graduationButton setBackgroundColor:[UIColor colorWithRed:244.0f/255.0f green:212.0f/255.0f blue:212.0f/255.0f alpha:1.0f]];
    [graduationButton setTitleColor:[UIColor colorWithRed:150.0f/255.0f green:150.0f/255.0f blue:150.0f/255.0f alpha:1.0f] forState:UIControlStateNormal];  
    
    [courseListController.textField setText:@""];
    [universityListController.textField setText:@""];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
	return NO;
}


@end
