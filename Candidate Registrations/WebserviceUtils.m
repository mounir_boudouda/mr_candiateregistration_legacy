

//
//  WebserviceUtils.m
//  Candidate Registrations
//
//  Created by Robert Taylor on 14/08/2011.
//  Copyright 2011 Home. All rights reserved.
//

#import "WebserviceUtils.h"


@implementation WebserviceUtils

- (id)init
{
    self = [super init];
    if (self) {
        // Initialization code here.
    }
    
    return self;
}

- (NSMutableArray*)downloadCourseList:(NSString*)serviceUrl:(NSString*)serverId:(NSString*)serverPassword
{
    NSMutableArray *courseList = nil;
    NSDictionary *feed = nil;
    
    @try {
        
         NSString *url = [NSString stringWithFormat:@"%@?providerId=%@&providerSecurityToken=%@",serviceUrl,serverId,serverPassword];
        
        id response = [self objectWithUrl:[NSURL URLWithString:url]];
    
        feed = (NSDictionary *)response;
        // get the array of "stream" from the feed and cast to NSArray
        NSArray *list = (NSArray *)[feed valueForKey:@"d"];
    
        courseList = [[NSMutableArray alloc] init];
    
        // loop over all the uni objects
        int loop;
        for (loop = 0; loop<list.count; loop++) {
            NSDictionary *uniItem = (NSDictionary *)[list objectAtIndex:loop];
        
            PopOverListItem *item = [[PopOverListItem alloc] init];
            item.rowId = [[uniItem valueForKey:@"Key"] intValue];
            NSString *description = [self cleanListDescription:[uniItem valueForKey:@"Value"]];
            [item setDescription:description];
            [courseList addObject:item];
        }
    }
    @catch (NSException * e) {
        NSLog(@"Exception: %@", e);
    }
    return courseList;  

}

- (NSMutableArray*)downloadUniversityList:(NSString*)serviceUrl:(NSString*)serverId:(NSString*)serverPassword
{
    NSMutableArray *uniList =nil;
    NSDictionary *feed = nil;
    @try {
        
        NSString *url = [NSString stringWithFormat:@"%@?providerId=%@&providerSecurityToken=%@",serviceUrl,serverId,serverPassword];
        
        id response = [self objectWithUrl:[NSURL URLWithString:url]];
        
        feed = (NSDictionary *)response;
        // get the array of "stream" from the feed and cast to NSArray
        NSArray *list = (NSArray *)[feed valueForKey:@"d"];
    
        uniList = [[NSMutableArray alloc] init];
    
        // loop over all the uni objects
        int loop;
        for (loop = 0; loop<list.count; loop++) {
            NSDictionary *uniItem = (NSDictionary *)[list objectAtIndex:loop];
        
            PopOverListItem *item = [[PopOverListItem alloc] init];
            item.rowId = [[uniItem valueForKey:@"Key"] intValue];
            NSString *description = [self cleanListDescription:[uniItem valueForKey:@"Value"]];
            [item setDescription:description];
            [uniList addObject:item];
        }
    }
    @catch (NSException * e) {
        NSLog(@"Exception: %@", e);
    }
    
    return uniList;
}

- (NSMutableArray*)downloadEventList:(NSString*)serviceUrl:(NSString*)serverId:(NSString*)serverPassword
{
    NSMutableArray *eventList = nil;
    NSDictionary *feed = nil;
    
    @try {
    
        NSString *url = [NSString stringWithFormat:@"%@?providerId=%@&providerSecurityToken=%@",serviceUrl,serverId,serverPassword];
        
        id response = [self objectWithUrl:[NSURL URLWithString:url]];
        
        feed = (NSDictionary *)response;
        // get the array of "stream" from the feed and cast to NSArray
        NSArray *list = (NSArray *)[feed valueForKey:@"d"];
    
        eventList = [[NSMutableArray alloc] init];
    
        // loop over all the uni objects
        int loop;
        for (loop = 0; loop<list.count; loop++) {
            NSDictionary *eventItem = (NSDictionary *)[list objectAtIndex:loop];
        
            CandidateEvent *item = [[CandidateEvent alloc] init];
            item.rowId = 0;
            NSString *description =  [[eventItem valueForKey:@"EventName"] stringByReplacingOccurrencesOfString:@"'" withString:@""];
            [item setEventName:description];
            item.eventId = [[eventItem valueForKey:@"Id"] intValue];

            NSString *dateStr = [eventItem valueForKey:@"StartDate"];
        
            // Convert string to date object
            //NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
            //[dateFormat setDateFormat:@"dd/MM/yyyy HH:mm:ss"];
            //NSDate *date = [dateFormat];
            
            NSDate *date = [self dateFromDotNetJSONString:dateStr];
            
            //[dateFormat release];
            [item setEventDate:date];
            [eventList addObject:item];
        }
        
    }
    @catch (NSException * e) {
        NSLog(@"Exception: %@", e);
    }
    
    return eventList;
}


- (int)registerCandidate:(NSString*)serverName:(CandidateDetails*)candidate:(NSString*)serverId:(NSString*)serverPassword
{
    if (candidate==nil) return false;
    NSDictionary *feed = nil;
    int result = 0;
    @try {
        
        NSString *url = [NSString stringWithFormat:@"%@/services/BulkUploadDataFactory.svc/SaveRegistration?providerId=%@&providerSecurityToken=%@&eventId=%d&courseId=%d&email=%@&firstName=%@&graduationYear=%d&lastName=%@&universityId=%d",serverName,serverId,serverPassword,candidate.eventId,candidate.courseId,candidate.email,candidate.firstName,candidate.graduationYear, candidate.lastName, candidate.universityId];
        
        id response = [self objectWithUrl:[NSURL URLWithString:[url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]];
    
        feed = (NSDictionary *)response;
        
        // get the array of "stream" from the feed and cast to NSArray
        NSArray *list = (NSArray *)[feed valueForKey:@"d"];
        // loop over all the uni objects
        int loop;
        for (loop = 0; loop<list.count; loop++)
        {
            NSDictionary *registrationItem = (NSDictionary *)[list objectAtIndex:loop];
            NSString *emailAddress = [registrationItem valueForKey:@"Key"];
            if ([emailAddress caseInsensitiveCompare:candidate.email] == NSOrderedSame)
            {
                result = [[registrationItem valueForKey:@"Value"] intValue];
            }
        }
        
    }
    @catch (NSException * e) {
        NSLog(@"Exception: %@", e);
    }
    return result;
}


- (NSString *)stringWithUrl:(NSURL *)url
{
	NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url
                                                cachePolicy:NSURLRequestReloadIgnoringCacheData
                                            timeoutInterval:30];
    // Fetch the JSON response
	NSData *urlData;
	NSURLResponse *response;
	NSError *error;
    
	// Make synchronous request
	urlData = [NSURLConnection sendSynchronousRequest:urlRequest
                                    returningResponse:&response
                                                error:&error];
    
 	// Construct a String around the Data from the response
	return [[NSString alloc] initWithData:urlData encoding:NSUTF8StringEncoding];
}

- (id) objectWithUrl:(NSURL *)url
{
    SBJsonParser *parser = [[SBJsonParser alloc]init];
	NSString *jsonString = [self stringWithUrl:url];
    NSDictionary *results = [parser objectWithString:jsonString];
	return results;
}

-(NSDate*)dateFromDotNetJSONString:(NSString *)string
{
    static NSRegularExpression *dateRegEx = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        dateRegEx = [[NSRegularExpression alloc] initWithPattern:@"^\\/date\\((-?\\d++)(?:([+-])(\\d{2})(\\d{2}))?\\)\\/$" options:NSRegularExpressionCaseInsensitive error:nil];
    });
    NSTextCheckingResult *regexResult = [dateRegEx firstMatchInString:string options:0 range:NSMakeRange(0, [string length])];
    
    if (regexResult)
    {
        // milliseconds
        NSTimeInterval seconds = [[string substringWithRange:[regexResult rangeAtIndex:1]] doubleValue] / 1000.0;
        // timezone offset
        if ([regexResult rangeAtIndex:2].location != NSNotFound) {
            NSString *sign = [string substringWithRange:[regexResult rangeAtIndex:2]];
            // hours
            seconds += [[NSString stringWithFormat:@"%@%@", sign, [string substringWithRange:[regexResult rangeAtIndex:3]]] doubleValue] * 60.0 * 60.0;
            // minutes
            seconds += [[NSString stringWithFormat:@"%@%@", sign, [string substringWithRange:[regexResult rangeAtIndex:4]]] doubleValue] * 60.0;
        }
        
        return [NSDate dateWithTimeIntervalSince1970:seconds];
    }
    return nil;
}

- (NSString *)cleanListDescription:(NSString *)searchTerm {
        // Setup an error to catch stuff in 
        NSError *error = NULL;
        //Create the regular expression to match against
        NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"[\\[0-9\\]]" 
                                                                               options:NSRegularExpressionCaseInsensitive 
                                                                                 error:&error];
        // create the new string by replacing the matching of the regex pattern with the template pattern(whitespace)
        NSString *newSearchString = [regex stringByReplacingMatchesInString:searchTerm options:0 
                                                                      range:NSMakeRange(0, [searchTerm length]) 
                                                               withTemplate:@""];
        
        return newSearchString;
    }

@end
