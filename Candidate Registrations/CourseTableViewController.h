//
//  UniversityTableViewController.h
//  Candidate Registrations
//
//  Created by Robert Taylor on 26/07/2011.
//  Copyright 2011 Home. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PopOverListItem.h"

@protocol CourseTableViewControllerDelegate <NSObject>

-(void)didCourseTap:(PopOverListItem*)item;

@end

@interface CourseTableViewController : UITableViewController
{
    NSMutableArray *courseListArray;
    __weak id popOverDelegate;
    NSMutableArray *localUniversityListArray;
    int eventId;
    NSMutableArray *searchResult;
    NSString *searchString;
}

@property (nonatomic, weak) id<CourseTableViewControllerDelegate> popOverDelegate;
@property (nonatomic, strong) NSMutableArray *courseListArray;
@property (nonatomic, strong) NSMutableArray *localCourseListArray;
@property (nonatomic, assign) int eventId;
@property (nonatomic, copy) NSString *searchString;

- (void) searchTableView:(NSString*)searchText;
- (PopOverListItem*) findCourse:(int)courseId;



@end
