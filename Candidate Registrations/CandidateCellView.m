//
//  CandidateCellView.m
//  Candidate Registrations
//
//  Created by Robert Taylor on 10/08/2011.
//  Copyright 2011 Home. All rights reserved.
//

#import "CandidateCellView.h"

@implementation CandidateCellView

@synthesize nameLabel;
@synthesize emailLabel;
@synthesize universityLabel;
@synthesize courseLabel;
@synthesize editButton;
@synthesize candidateDetails;
@synthesize graduationLabel;


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)initCandidate
{
    NSString *nameString = [NSString stringWithFormat:@"%@ %@", candidateDetails.firstName, candidateDetails.lastName];
    NSString *emailString = candidateDetails.email;
    NSString *universityString = candidateDetails.universityName;
    NSString *courseString = candidateDetails.courseName;
    NSString *graduationString = [NSString stringWithFormat:@"Graduating in %d", candidateDetails.graduationYear];
    
    [nameLabel setText:nameString];  
    [emailLabel setText:emailString];
    [universityLabel setText:universityString];
    [courseLabel setText:courseString];
    if (candidateDetails.uploadStatus !=1)
        [editButton setTag:candidateDetails.rowId];
    else
        [editButton setTag:0];
    if (candidateDetails.uploadStatus>0)
        [editButton setBackgroundImage:[UIImage imageNamed:@"small-tick.png"] forState:UIControlStateNormal];        
    else    
        [editButton setBackgroundImage:[UIImage imageNamed:@"small-cross.png"] forState:UIControlStateNormal]; 
    if (candidateDetails.graduationYear>0)
        [graduationLabel setText:graduationString];
    else
        [graduationLabel setText:@""];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
