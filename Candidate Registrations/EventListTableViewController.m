//
//  EventListTableViewController.m
//  Candidate Registrations
//
//  Created by Robert Taylor on 17/07/2011.
//  Copyright 2011 Home. All rights reserved.
//

#import "EventListTableViewController.h"
#import "DataUtils.h"
#import "CandidateEvent.h"
#import "Candidate_RegistrationsAppDelegate.h"
#import "QuartzCore/QuartzCore.h"

@implementation EventListTableViewController

@synthesize isArchive;
@synthesize tableCell;
@synthesize dataEntryViewController;
@synthesize candidateListViewController;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
        
    // Uncomment the following line to preserve selection between presentations.
    self.clearsSelectionOnViewWillAppear = NO;
    
    self.tableView.rowHeight = 72;
    self.tableView.backgroundColor = [UIColor whiteColor];
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [self reloadTable];
    [super viewWillAppear:animated];
}

-(void)reloadTable
{
    DataUtils *dataUtils = [[DataUtils alloc] init];
    
    eventsList = [dataUtils loadEventsList:self.isArchive];
    [self.tableView reloadData];   

}

-(void)dataEntryViewDidClose
{
    [self viewWillAppear:false];
}

-(void)candidateListViewDidClose
{
    [self viewWillAppear:false];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
	return NO;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    if (eventsList!=nil)
        return [eventsList count];
    else
        return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    EventCellView *cell = (EventCellView *) [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if(cell == nil) 
    {
        
        [[NSBundle mainBundle] loadNibNamed:@"EventCellView" owner:self options:nil];
        cell = tableCell;
        
        [cell.updateButton addTarget:self action:@selector(updateButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
        [cell.dateEntryButton addTarget:self action:@selector(dataEntryButtonPressed:) forControlEvents:UIControlEventTouchUpInside];

        // Configure the cell...
        CandidateEvent *event = (CandidateEvent *)[eventsList objectAtIndex:indexPath.row];
        cell.candidateEvent = event;
        cell.dateEntryButton.tag = event.eventId;
        cell.updateButton.tag = event.eventId;
        cell.deleteButton.tag = event.eventId;
        cell.eventId = event.eventId;
        [cell initEvent];
    }
    return cell;
}

-(IBAction)updateButtonPressed:(id)sender
{
    candidateListViewController = [[CandidateListViewController alloc] initWithNibName:@"CandidateListViewController" bundle:nil];
    
    candidateListViewController.eventId = ((UIButton*)sender).tag;
    candidateListViewController.delegate = self;
    
    Candidate_RegistrationsAppDelegate *appDelegate = (Candidate_RegistrationsAppDelegate *)[[UIApplication sharedApplication] delegate];
    
    [appDelegate.window addSubview:candidateListViewController.view];   
    
    // set up an animation for the transition between the views
	CATransition *animation = [CATransition animation];
	[animation setDuration:0.5];
	[animation setType:kCATransitionFade];
	[animation setSubtype:kCATransitionFromLeft];
	[animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
	
	[[appDelegate.window layer] addAnimation:animation forKey:@"SwitchToViewCandidateListView"];
}

-(IBAction)deleteButtonPressed:(id)sender
{
    int eventId = ((UIButton*)sender).tag;
    CandidateEvent *candidateEvent = [eventsList objectAtIndex:eventId];
    DataUtils *dataUtils = [[DataUtils alloc] init];
    [dataUtils deleteEventItem:candidateEvent];
    
    [self reloadTable];
}

-(IBAction)dataEntryButtonPressed:(id)sender
{
    dataEntryViewController = [[DataEntryViewController alloc] initWithNibName:@"DataEntryViewController" bundle:nil];
    
    dataEntryViewController.eventId = ((UIButton*)sender).tag;
    dataEntryViewController.delegate = self;
    
    Candidate_RegistrationsAppDelegate *appDelegate = (Candidate_RegistrationsAppDelegate *)[[UIApplication sharedApplication] delegate];

    [appDelegate.window addSubview:dataEntryViewController.view];   
    
    // set up an animation for the transition between the views
	CATransition *animation = [CATransition animation];
	[animation setDuration:0.5];
	[animation setType:kCATransitionFade];
	[animation setSubtype:kCATransitionFromLeft];
	[animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
	
	[[appDelegate.window layer] addAnimation:animation forKey:@"SwitchToView1"];
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/


// Override to support editing the table view.
/*- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        //[tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
        
        CandidateEvent *candidateEvent = [eventsList objectAtIndex:[indexPath row]];
        DataUtils *dataUtils = [[DataUtils alloc] init];
        [dataUtils deleteEventItem:candidateEvent];
        [dataUtils release];
        
        // Reload the view
        [self viewWillAppear:true];

    }   
}*/


/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     [detailViewController release];
     */
}

@end
