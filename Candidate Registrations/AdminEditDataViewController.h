//
//  AdminEditDataViewController.h
//  Candidate Registrations
//
//  Created by Robert Taylor on 06/09/2011.
//  Copyright 2011 Home. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UniversityTableViewController.h"
#import "CourseTableViewController.h"
#import "GraduationYearTableViewController.h"
#import "InputViewKeyboard.h"
#import "PopOverListItem.h"
#import "FeedbackMessageOverlay.h"
#import "Constants.h"
#import "CandidateDetails.h"

@protocol EditDataViewDelegate
- (void)editDataViewDidClose;
@end

@interface AdminEditDataViewController : UIViewController<UniversityTableViewControllerDelegate,CourseTableViewControllerDelegate, GraduationYearPopOverControllerDelegate, UITextViewDelegate>

{
    UIButton *closeButton;
    UIButton *registerButton;
    UIButton *universityButton;
    UIButton *courseButton;
    UIButton *graduationButton;
    UIPopoverController *universityPopOverController;
    UIPopoverController *coursePopOverController;
    UIPopoverController *graduationYearPopOverController;

    UniversityTableViewController *universityListController;    
    CourseTableViewController *courseListController;  
    GraduationYearTableViewController *graduationYearListController;
    InputViewKeyboard *firstNameKeyboardView;
    InputViewKeyboard *lastNameKeyboardView;
    InputViewKeyboard *emailNameKeyboardView;

    int candidateId;
    CandidateDetails *candidateDetails;
    
    PopOverListItem *defaultUniversityPopOverItem;
    PopOverListItem *defaultCoursePopOverItem;
    PopOverListItem *defaultGraduationYearPopOverItem;
    FeedbackMessageOverlay *messageOverlay;

    __weak id <NSObject, EditDataViewDelegate> delegate;

}

@property (nonatomic, strong) IBOutlet UIButton *closeButton;
@property (nonatomic, strong) IBOutlet UIButton *registerButton;
@property (nonatomic, strong) IBOutlet UIButton *universityButton;
@property (nonatomic, strong) IBOutlet UIButton *courseButton;
@property (nonatomic, strong) IBOutlet UIButton *graduationButton;
@property (nonatomic, strong) UIPopoverController *universityPopOverController;
@property (nonatomic, strong) UIPopoverController *coursePopOverController;
@property (nonatomic, strong) UIPopoverController *graduationYearPopOverController;
@property (nonatomic, strong) IBOutlet UniversityTableViewController *universityListController;
@property (nonatomic, strong) IBOutlet CourseTableViewController *courseListController;
@property (nonatomic, strong) IBOutlet GraduationYearTableViewController *graduationYearListController;
@property (nonatomic, strong) IBOutlet InputViewKeyboard *firstNameKeyboardView;
@property (nonatomic, strong) IBOutlet InputViewKeyboard *emailNameKeyboardView;
@property (nonatomic, strong) IBOutlet InputViewKeyboard *lastNameKeyboardView;
@property (nonatomic, assign) int candidateId;
@property (nonatomic, strong) CandidateDetails *candidateDetails;
@property (nonatomic, strong) PopOverListItem *defaultCoursePopOverItem;
@property (nonatomic, strong) PopOverListItem *defaultUniversityPopOverItem;
@property (nonatomic, strong) PopOverListItem *defaultGraduationYearPopOverItem;
@property (nonatomic, strong) FeedbackMessageOverlay *messageOverlay;

@property (nonatomic, weak) id <NSObject, EditDataViewDelegate> delegate;

-(IBAction)toggleEditCandidateUniversityPopOverController;
-(IBAction)toggleEditCandidateCoursePopOverController;
-(IBAction)toggleEditCandidateGraduationYearPopOverController;
-(IBAction)saveEditedCandidateButtonClicked:(id)sender;
-(IBAction)editDataViewCloseButtonClicked:(id)sender;
-(IBAction)textViewDidBeginEditing:(UITextView *)textView;
-(IBAction)textViewDidEndEditing:(UITextView *)textView;

@end
