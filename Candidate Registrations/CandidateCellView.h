//
//  CandidateCellView.h
//  Candidate Registrations
//
//  Created by Robert Taylor on 10/08/2011.
//  Copyright 2011 Home. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CandidateDetails.h"

@interface CandidateCellView : UITableViewCell
{
    UILabel *nameLabel;
    UILabel *emailLabel;
    UILabel *universityLabel;
    UILabel *courseLabel;
    UILabel *graduationLabel;
    UIButton *editButton;
    CandidateDetails *candidateDetails;
}

@property (nonatomic, strong) IBOutlet UILabel *nameLabel;
@property (nonatomic, strong) IBOutlet UILabel *emailLabel;
@property (nonatomic, strong) IBOutlet UILabel *universityLabel;
@property (nonatomic, strong) IBOutlet UILabel *courseLabel;
@property (nonatomic, strong) IBOutlet UILabel *graduationLabel;
@property (nonatomic, strong) IBOutlet UIButton *editButton;
@property (nonatomic, strong) CandidateDetails *candidateDetails;

-(void)initCandidate;
@end
