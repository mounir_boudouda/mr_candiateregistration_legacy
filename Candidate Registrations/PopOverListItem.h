//
//  PopOverListItem.h
//  Candidate Registrations
//
//  Created by Robert Taylor on 26/07/2011.
//  Copyright 2011 Home. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PopOverListItem : NSObject
{
    NSString *description;
    int rowId;
}

@property(nonatomic,copy) NSString *description;
@property(readwrite,assign) int rowId;

@end
