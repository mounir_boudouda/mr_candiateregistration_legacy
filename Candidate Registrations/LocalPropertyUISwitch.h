//
//  LocalUniversityUISwitch.h
//  Candidate Registrations
//
//  Created by Robert Taylor on 19/08/2011.
//  Copyright 2011 Home. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LocalPropertyUISwitch : UISwitch
{
    int EventId;
    int PropertyId;
}

@property (readwrite, assign) int EventId;
@property (readwrite, assign) int PropertyId;
@end
