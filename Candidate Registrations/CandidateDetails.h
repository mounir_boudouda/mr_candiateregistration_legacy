//
//  CandidateDetails.h
//  Candidate Registrations
//
//  Created by Robert Taylor on 09/08/2011.
//  Copyright 2011 Home. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CandidateDetails : NSObject
{
    int rowId;
    int eventId;
    int universityId;
    int courseId;
    NSString *firstName;
    NSString *lastName;
    NSString *email;
    NSString *universityName;
    NSString *courseName;
    int uploadStatus;
    int graduationYear;
    NSDate *dateCreated;
    NSDate *uploadDate;
}

@property(readwrite,assign) int rowId;
@property(readwrite,assign) int eventId;
@property(readwrite,assign) int universityId;
@property(readwrite,assign) int courseId;
@property(readwrite,assign) int uploadStatus;
@property(readwrite,assign) int graduationYear;
@property(nonatomic,copy) NSString *firstName;
@property(nonatomic,copy) NSString *lastName;
@property(nonatomic,copy) NSString *email;
@property(nonatomic,copy) NSString *universityName;
@property(nonatomic,copy) NSString *courseName;
@property(nonatomic,copy) NSDate *dateCreated;
@property(nonatomic,copy) NSDate *uploadDate;

@end
