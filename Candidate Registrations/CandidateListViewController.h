//
//  CandidateListViewController.h
//  Candidate Registrations
//
//  Created by Robert Taylor on 09/08/2011.
//  Copyright 2011 Home. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CandidateTableViewController.h"
#import "LocalPropertyViewController.h"
#import "FeedbackMessageOverlay.h"

@protocol CandidateListViewDelegate
- (void)candidateListViewDidClose;
@end

@interface CandidateListViewController : UIViewController
{
    int eventId;
    CandidateTableViewController *tableViewController;
    LocalPropertyViewController *localPropertyViewController;
    UIView *tableView;
    UIButton *uploadButton;
    UIButton *backButton;
    UIButton *localUniversitiesButton;
    UIButton *localCoursesButton;
    UILabel *eventNameLabel;
    UILabel *totalCountLabel;
    UILabel *notUploadedLabel;
    UILabel *uploadedLabel;
    BOOL isDataUploadSuccessful;
    FeedbackMessageOverlay *messageOverlay;
    id <NSObject, CandidateListViewDelegate> delegate;
}

@property (nonatomic, assign) int eventId;
@property (nonatomic, strong) IBOutlet CandidateTableViewController *tableViewController;
@property (nonatomic, strong) IBOutlet LocalPropertyViewController *localPropertyViewController;
@property (nonatomic, strong) IBOutlet UIView *tableView; 
@property (nonatomic, strong) IBOutlet UIButton *uploadButton;
@property (nonatomic, strong) IBOutlet UIButton *backButton;
@property (nonatomic, strong) IBOutlet UIButton *localUniversitiesButton;
@property (nonatomic, strong) IBOutlet UIButton *localCoursesButton;
@property (nonatomic, strong) IBOutlet UILabel *eventNameLabel;
@property (nonatomic, strong) IBOutlet UILabel *totalCountLabel;
@property (nonatomic, strong) IBOutlet UILabel *notUploadedLabel;
@property (nonatomic, strong) IBOutlet UILabel *uploadedLabel;
@property (nonatomic, assign) BOOL isDataUploadSuccessful;
@property (nonatomic, strong) FeedbackMessageOverlay *messageOverlay;
@property (strong) id <NSObject, CandidateListViewDelegate> delegate;

- (void)reloadView;
- (void)displayLocalPropertiesScreen:(bool)isUniversity;
- (IBAction)localCoursesButtonClicked:(id)sender;
- (IBAction)localUniversitiesButtonClicked:(id)sender;

@end
