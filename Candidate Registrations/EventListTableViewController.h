//
//  EventListTableViewController.h
//  Candidate Registrations
//
//  Created by Robert Taylor on 17/07/2011.
//  Copyright 2011 Home. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EventCellView.h"
#import "DataEntryViewController.h"
#import "CandidateListViewController.h"

@interface EventListTableViewController : UITableViewController <DataEntryViewDelegate,CandidateListViewDelegate> {
    NSMutableArray *eventsList;
    EventCellView *tableCell;
    bool isArchive;
    DataEntryViewController *dataEntryViewController;
    CandidateListViewController *candidateListViewController;
}

@property (readwrite,assign) bool isArchive;
@property (nonatomic, strong) IBOutlet EventCellView *tableCell;
@property (nonatomic, strong) IBOutlet DataEntryViewController *dataEntryViewController;
@property (nonatomic, strong) IBOutlet CandidateListViewController *candidateListViewController;

-(IBAction)updateButtonPressed:(id)sender;
-(IBAction)dataEntryButtonPressed:(id)sender;
-(IBAction)deleteButtonPressed:(id)sender;
-(void)dataEntryViewDidClose;
-(void)candidateListViewDidClose;
-(void)reloadTable;

@end
