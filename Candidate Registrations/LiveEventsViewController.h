//
//  LiveEventsViewController.h
//  Candidate Registrations
//
//  Created by Robert Taylor on 17/07/2011.
//  Copyright 2011 Home. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EventListTableViewController.h"


@interface LiveEventsViewController : UIViewController {
    EventListTableViewController *tableListController;
    UIView *tableContainerView;
}

@property (nonatomic, strong) IBOutlet EventListTableViewController *tableListController;
@property (nonatomic, strong) IBOutlet UIView *tableContainerView;

@end
