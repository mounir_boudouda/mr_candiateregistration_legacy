//
//  SelectUniversityListViewController.h
//  Candidate Registrations
//
//  Created by Afzal Valiji on 21/08/2012.
//  Copyright 2012 Home. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UniversityTableViewController.h"
#import "InputViewKeyboard.h"
@protocol SelectUniversityListViewControllerDelegate <NSObject>

-(void)didUniversityTap:(PopOverListItem*)item;

@end

@interface SelectUniversityListViewController : UIViewController<UniversityTableViewControllerDelegate>
{
    __weak id popOverDelegate;
    UniversityTableViewController  *tableListController;
    UIView *tableContainerView;
    InputViewKeyboard *textField;
    int eventId;
}

@property (nonatomic, weak) id<SelectUniversityListViewControllerDelegate> popOverDelegate;
@property (nonatomic, strong) IBOutlet UniversityTableViewController *tableListController;
@property (nonatomic, strong) IBOutlet UIView *tableContainerView;
@property (nonatomic, strong) IBOutlet InputViewKeyboard *textField;
@property (nonatomic, assign) int eventId;

-(void)didUniversityTap:(PopOverListItem *)item;
-(void)didEditText;

@end
