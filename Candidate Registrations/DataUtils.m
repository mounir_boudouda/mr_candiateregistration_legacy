//
//  DataUtils.m
//  Candidate Registrations
//
//  Created by Robert Taylor on 17/07/2011.
//  Copyright 2011 Home. All rights reserved.
//

#import "DataUtils.h"
#import "CandidateEvent.h"
#import "CandidateDetails.h"
#import "PopOverListItem.h"
#import "ErrorLogItem.h"
#import "EventLocalProperty.h"



@implementation DataUtils

+(NSString *) dataFilePath
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *writableDBPath = [documentsDirectory stringByAppendingPathComponent:@"milkround.sqlite"];
    return writableDBPath;
}


+(BOOL) IsDBCreated
{  
    BOOL exists = false;
    @try {
        
           NSFileManager *fileManager = [NSFileManager defaultManager];
        NSError *error;
        exists = [fileManager fileExistsAtPath:[DataUtils dataFilePath]];
        if (!exists)
        {
            NSString *defaultDBPath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"milkround.sqlite"];
        
            exists = [fileManager copyItemAtPath:defaultDBPath toPath:[DataUtils dataFilePath] error:&error];
        }
    }
    @catch (NSException * e) {
        NSLog(@"Exception: %@", e);
    }
  
    return exists;
}

+(sqlite3 *) getNewDBConnection{
    
    if ([DataUtils IsDBCreated] == false) return nil;
    
    sqlite3 *newDBconnection;
    // Open the database. The database was prepared outside the application.
    if (sqlite3_open([[DataUtils dataFilePath] UTF8String], &newDBconnection) == SQLITE_OK) {
        
        NSLog(@"Database Successfully Opened");
    } 
    return newDBconnection;
}

- (NSMutableArray *) loadEventLocalCourseList:(int)eventId
{
    sqlite3 *database = [DataUtils getNewDBConnection];
    if (database == nil) return nil;
        
    NSString *query = [NSString stringWithFormat: @"SELECT Id, EventId, CourseID, DisplayOrder FROM Event_Courses WHERE EventId=%d",eventId];
  
    sqlite3_stmt *statement;
    NSMutableArray *array = [[NSMutableArray alloc] init];
        
    if (sqlite3_prepare_v2(database, [query UTF8String], -1, &statement, nil) == SQLITE_OK)
    {
        while (sqlite3_step(statement)==SQLITE_ROW) {
                
            EventLocalProperty *event = [[EventLocalProperty alloc] init];
            event.rowId = sqlite3_column_int(statement, 0);
            event.eventId = sqlite3_column_int(statement, 1);
            event.propertyId = sqlite3_column_int(statement, 2);
            event.displayOrder = sqlite3_column_int(statement, 3);
            [array addObject:event];
        }
    }
    else
    {
        [self saveError: [NSString stringWithFormat: @"Error: failed to get event local course list %s.", sqlite3_errmsg(database)]];
    }
    sqlite3_finalize(statement);
    sqlite3_close(database);
    return array;
}


- (BOOL) insertEventLocalCourse:(int)eventId:(int)courseId:(int)displayOrder
{
    bool state = false;
    sqlite3 *database = [DataUtils getNewDBConnection];
    
    if (database == nil) return state;
    
    NSString *query = [NSString stringWithFormat: @"INSERT INTO Event_Courses (EventId, CourseID, DisplayOrder) values (%d,%d,%d)", eventId, courseId, displayOrder];
    
    sqlite3_stmt *statement;
    if (sqlite3_prepare_v2(database, [query UTF8String], -1, &statement, nil) == SQLITE_OK)
    {
        if (SQLITE_DONE != sqlite3_step(statement))
            state = false;
        else
            state = true;
    }
    else
    {
        state = false;
        [self saveError: [NSString stringWithFormat: @"Error: failed to get save local course %s.", sqlite3_errmsg(database)]];
        
    }
    sqlite3_finalize(statement);
    sqlite3_close(database);
    return state;  
}



- (BOOL) removeEventLocalCourse:(int)eventId:(int)courseId
{
    bool state = false;
    sqlite3 *database = [DataUtils getNewDBConnection];
    
    if (database == nil) return state;
    
    NSString *query = [NSString stringWithFormat: @"DELETE FROM Event_Courses WHERE CourseID=%d AND EventId=%d", courseId, eventId];
    
    sqlite3_stmt *statement;
    if (sqlite3_prepare_v2(database, [query UTF8String], -1, &statement, nil) == SQLITE_OK)
    {
        if (SQLITE_DONE != sqlite3_step(statement))
            state = false;
        else
            state = true;
    }
    else
    {
        state = false;
        [self saveError: [NSString stringWithFormat: @"Error: failed to get remove local course '%s'.", sqlite3_errmsg(database)]];
        
    }
    sqlite3_finalize(statement);
    sqlite3_close(database);
    return state; 
}




- (NSMutableArray *) loadEventLocalUniversityList:(int)eventId
{
    sqlite3 *database = [DataUtils getNewDBConnection];
    if (database == nil) return nil;
    
    NSString *query = [NSString stringWithFormat: @"SELECT Id, EventId, UniversityId, DisplayOrder FROM Event_Universities WHERE EventId=%d",eventId];
    
    sqlite3_stmt *statement;
    NSMutableArray *array = [[NSMutableArray alloc] init];
    
    if (sqlite3_prepare_v2(database, [query UTF8String], -1, &statement, nil) == SQLITE_OK)
    {
        while (sqlite3_step(statement)==SQLITE_ROW) {
            
            EventLocalProperty *event = [[EventLocalProperty alloc] init];
            event.rowId = sqlite3_column_int(statement, 0);
            event.eventId = sqlite3_column_int(statement, 1);
            event.propertyId = sqlite3_column_int(statement, 2);
            event.displayOrder = sqlite3_column_int(statement, 3);
            [array addObject:event];
        }
    }
    else
    {
        [self saveError: [NSString stringWithFormat: @"Error: failed to get event local university list %s.", sqlite3_errmsg(database)]];
    }
    sqlite3_finalize(statement);
    sqlite3_close(database);
    return array;
}

- (BOOL) insertEventLocalUniversity:(int)eventId:(int)universityId:(int)displayOrder
{
    bool state = false;
    sqlite3 *database = [DataUtils getNewDBConnection];
    
    if (database == nil) return state;
    
    NSString *query = [NSString stringWithFormat: @"INSERT INTO Event_Universities (EventId, UniversityId, DisplayOrder) values (%d,%d,%d)", eventId, universityId, displayOrder];
    
    sqlite3_stmt *statement;
    if (sqlite3_prepare_v2(database, [query UTF8String], -1, &statement, nil) == SQLITE_OK)
    {
        if (SQLITE_DONE != sqlite3_step(statement))
            state = false;
        else
            state = true;
    }
    else
    {
        state = false;
        [self saveError: [NSString stringWithFormat: @"Error: failed to get save local university %s.", sqlite3_errmsg(database)]];
        
    }
    sqlite3_finalize(statement);
    sqlite3_close(database);
    return state;  
}

- (BOOL) removeEventLocalUniversity:(int)eventId:(int)universityId
{
    bool state = false;
    sqlite3 *database = [DataUtils getNewDBConnection];
    
    if (database == nil) return state;
    
    NSString *query = [NSString stringWithFormat: @"DELETE FROM Event_Universities WHERE UniversityId=%d AND EventId=%d", universityId, eventId];
    
    sqlite3_stmt *statement;
    if (sqlite3_prepare_v2(database, [query UTF8String], -1, &statement, nil) == SQLITE_OK)
    {
        if (SQLITE_DONE != sqlite3_step(statement))
            state = false;
        else
            state = true;
    }
    else
    {
        state = false;
        [self saveError: [NSString stringWithFormat: @"Error: failed to get remove local university '%s'.", sqlite3_errmsg(database)]];
        
    }
    sqlite3_finalize(statement);
    sqlite3_close(database);
    return state; 
}



//////////////////////////////////////////////////////////////////
//
// Method to get a list of events from DB. Will return array of
// CandidateEvent objects or nil (if can't access DB)
//
//////////////////////////////////////////////////////////////////
-(NSMutableArray *) loadEventsList:(BOOL) isArchive
{
    sqlite3 *database = [DataUtils getNewDBConnection];
    if (database == nil) return nil;

    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSString *dateString = [NSString stringWithFormat:@"%@", [dateFormatter stringFromDate:[NSDate date]]];
    
    
    NSString *query = @"SELECT Id, EventName, EventId, EventDate FROM Events WHERE EventDate";
    if (isArchive)
        query = [query stringByAppendingFormat:@" < '%@ 00:00:00' ORDER BY EventDate DESC", dateString]; 
    else
        query = [query stringByAppendingFormat:@" >= '%@ 00:00:00' ORDER BY EventDate ASC", dateString];
       
    sqlite3_stmt *statement;
    NSMutableArray *array = [[NSMutableArray alloc] init];

    if (sqlite3_prepare_v2(database, [query UTF8String], -1, &statement, nil) == SQLITE_OK)
    {
        while (sqlite3_step(statement)==SQLITE_ROW) {
            
            CandidateEvent *event = [[CandidateEvent alloc] init];
            event.rowId = sqlite3_column_int(statement, 0);
            NSString *eventName = [NSString stringWithUTF8String: (char *)sqlite3_column_text(statement, 1)];
            [event setEventName: eventName];
            event.eventId = sqlite3_column_int(statement, 2);
            
            NSString *dateStr = [NSString stringWithUTF8String: (char *)sqlite3_column_text(statement, 3)];
            
            // Convert string to date object
            NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
            [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
            
            NSDate *date = [dateFormat dateFromString:dateStr];  
            [event setEventDate: date];

            [array addObject:event];
            
        }
    }
    else
    {
        [self saveError: [NSString stringWithFormat: @"Error: failed to get events list %s.", sqlite3_errmsg(database)]];
    }
    sqlite3_finalize(statement);
    sqlite3_close(database);
    return array;
}

- (CandidateEvent *)loadEventByEventId:(int)eventId
{
    CandidateEvent *event = [[CandidateEvent alloc] init];
    sqlite3 *database = [DataUtils getNewDBConnection];
    if (database == nil) return nil;
    
    NSString *query = [NSString stringWithFormat: @"SELECT Id, EventName, EventId, EventDate FROM Events WHERE EventId=%d",eventId];
    
    sqlite3_stmt *statement;
    
    if (sqlite3_prepare_v2(database, [query UTF8String], -1, &statement, nil) == SQLITE_OK)
    {
        while (sqlite3_step(statement)==SQLITE_ROW) {
            event.rowId = sqlite3_column_int(statement, 0);
            NSString *eventName = [NSString stringWithUTF8String: (char *)sqlite3_column_text(statement, 1)];
            [event setEventName: eventName];
            event.eventId = sqlite3_column_int(statement, 2);
            
            NSString *dateStr = [NSString stringWithUTF8String: (char *)sqlite3_column_text(statement, 3)];
            
            // Convert string to date object
            NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
            [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
            
            NSDate *date = [dateFormat dateFromString:dateStr];  
            [event setEventDate: date];
            
        }
    }
    else
    {
        [self saveError: [NSString stringWithFormat: @"Error: failed to get events with ID %d %s.", eventId, sqlite3_errmsg(database)]];
    }
    sqlite3_finalize(statement);
    sqlite3_close(database);
   
    return event;
}


- (BOOL)saveEventItem:(CandidateEvent*) eventItem
{
    bool state = false;
    sqlite3 *database = [DataUtils getNewDBConnection];
    
    if (database == nil) return state;
    
    NSString *query;
    
    CandidateEvent *oldEventItem = [self loadEventByEventId:eventItem.eventId];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *eventDate = [formatter stringFromDate:eventItem.eventDate];
    
    if (oldEventItem == nil || oldEventItem.rowId==0)
    {
        query = [NSString stringWithFormat: @"INSERT INTO Events (EventName, EventId, EventDate) values ('%@', %d,'%@')", eventItem.eventName, eventItem.eventId, eventDate ];
    }
    else
    {
        query = [NSString stringWithFormat: @"UPDATE Events SET EventName = '%@', EventDate = '%@' WHERE EventId=%d", eventItem.eventName, eventDate, eventItem.eventId];
        
        
    }
    sqlite3_stmt *statement;
    if (sqlite3_prepare_v2(database, [query UTF8String], -1, &statement, nil) == SQLITE_OK)
    {
        if (SQLITE_DONE != sqlite3_step(statement))
            state = false;
        else
            state = true;
    }
    else
    {
        state = false;
    }
    sqlite3_finalize(statement);
    sqlite3_close(database);
    return state; 
}




- (BOOL) deleteEventItem:(CandidateEvent*) eventItem
{
    
    bool state = false;
    sqlite3 *database = [DataUtils getNewDBConnection];
    
    if (database == nil) return state;
    
    NSString *queryDeleteEvent;
    
    CandidateEvent *oldEventItem = [self loadEventByEventId:eventItem.eventId];
    
    if (oldEventItem != nil)
    {
        queryDeleteEvent = [NSString stringWithFormat: @"DELETE FROM Events WHERE EventId=%d; DELETE FROM Registrations WHERE EventId=%d;", eventItem.eventId, eventItem.eventId];
        sqlite3_stmt *statement;
        if (sqlite3_prepare_v2(database, [queryDeleteEvent UTF8String], -1, &statement, nil) == SQLITE_OK)
        {
            if (SQLITE_DONE != sqlite3_step(statement))
                state = false;
            else
                state = true;
        }
        else
        {
            state = false;
        }
    }

    sqlite3_close(database);
    return state; 
}


- (PopOverListItem *)loadUniversityItem:(int)universityId
{
    PopOverListItem *item = [[PopOverListItem alloc] init];
    sqlite3 *database = [DataUtils getNewDBConnection];
    if (database == nil) return nil;
    
    NSString *query = [NSString stringWithFormat: @"SELECT Id, Description FROM Universities WHERE Id=%d",universityId];
    
    sqlite3_stmt *statement;
    
    if (sqlite3_prepare_v2(database, [query UTF8String], -1, &statement, nil) == SQLITE_OK)
    {
        while (sqlite3_step(statement)==SQLITE_ROW) {
            item.rowId = sqlite3_column_int(statement, 0);
            NSString *universityName = [NSString stringWithUTF8String: (char *)sqlite3_column_text(statement, 1)];
            [item setDescription: universityName];
        }
    }
    
    sqlite3_finalize(statement);
    sqlite3_close(database);
    
    return item;
}

- (int) getCandidateCounts:(BOOL)isUploaded:(int)eventId
{
    sqlite3 *database = [DataUtils getNewDBConnection];
    if (database == nil) return 0;
    
    NSString *query = nil;
    int count = 0;
    
    if (isUploaded)
        query = [NSString stringWithFormat: @"SELECT COUNT(ID) AS 'COUNT' FROM Registrations WHERE EventId=%d AND UploadStatus=1",eventId];
    else
        query = [NSString stringWithFormat: @"SELECT COUNT(ID) AS 'COUNT' FROM Registrations WHERE EventId=%d AND UploadStatus=0",eventId];
    
    sqlite3_stmt *statement;
    
    if (sqlite3_prepare_v2(database, [query UTF8String], -1, &statement, nil) == SQLITE_OK)
    {
        while (sqlite3_step(statement)==SQLITE_ROW) {
            count = sqlite3_column_int(statement, 0);
        }
    }
    else
    {
        if (database!=nil)
        {
            [self saveError: [NSString stringWithFormat: @"Error: failed to get upload counts %s.", sqlite3_errmsg(database)]];
        }
    }
    
    sqlite3_finalize(statement);
    sqlite3_close(database);
    
    return count;
}

- (BOOL)deleteUniversities
{
    bool state = false;
    sqlite3 *database = [DataUtils getNewDBConnection];
    
    if (database == nil) return state;
    
    NSString *query;
    query = [NSString stringWithFormat: @"DELETE FROM Universities" ];
    
    sqlite3_stmt *statement;
    if (sqlite3_prepare_v2(database, [query UTF8String], -1, &statement, nil) == SQLITE_OK)
    {
        if (SQLITE_DONE != sqlite3_step(statement))
            state = false;
        else
            state = true;
    }
    else
    {
        state = false;
    }
    sqlite3_finalize(statement);
    sqlite3_close(database);
    return state; 
}

- (BOOL)saveUniversityItem:(PopOverListItem*) universityItem
{
    bool state = false;
    sqlite3 *database = [DataUtils getNewDBConnection];
    
    if (database == nil) return state;
    
    NSString *query;
    
    PopOverListItem *oldUniItem = [self loadUniversityItem:universityItem.rowId];
    
    if (oldUniItem == nil || oldUniItem.rowId==0)
    {
        query = [NSString stringWithFormat: @"INSERT INTO Universities (Id, Description) values (%d,'%@')", universityItem.rowId, universityItem.description ];
    }
    else
    {
        query = [NSString stringWithFormat: @"UPDATE Universities SET Description = '%@' WHERE Id=%d", universityItem.description, universityItem.rowId ];
        

    }
    sqlite3_stmt *statement;
    if (sqlite3_prepare_v2(database, [query UTF8String], -1, &statement, nil) == SQLITE_OK)
    {
        if (SQLITE_DONE != sqlite3_step(statement))
            state = false;
        else
            state = true;
    }
    else
    {
        state = false;
    }
    sqlite3_finalize(statement);
    sqlite3_close(database);
    return state; 
}

-(NSMutableArray*)loadUniversityList
{
    sqlite3 *database = [DataUtils getNewDBConnection];
    if (database == nil) return nil;
      
    NSString *query = @"SELECT Id, Description FROM Universities ORDER BY Description";
    
    sqlite3_stmt *statement;
    NSMutableArray *array = [[NSMutableArray alloc] init];
    
    if (sqlite3_prepare_v2(database, [query UTF8String], -1, &statement, nil) == SQLITE_OK)
    {
        while (sqlite3_step(statement)==SQLITE_ROW) {
            
            PopOverListItem *item = [[PopOverListItem alloc] init];
            item.rowId = sqlite3_column_int(statement, 0);
            NSString *description = [NSString stringWithUTF8String: (char *)sqlite3_column_text(statement, 1)];
            [item setDescription:description];
            [array addObject:item];
            
        }        
    }
    else
    {
        [self saveError: [NSString stringWithFormat: @"Error: failed to get university list %s.", sqlite3_errmsg(database)]];
    }
    sqlite3_finalize(statement);
    sqlite3_close(database);
    return array;

}

- (BOOL) syncCourseList:(NSString*)url
{
    WebserviceUtils *webservice = [[WebserviceUtils alloc] init];
    BOOL success=FALSE;
    url = [NSString stringWithFormat:@"%@%@", [self getServerName], url];
    NSMutableArray *list = [webservice downloadCourseList:url:[self getProviderId]:[self getProviderPassword]];
    
    if (list!=nil && list.count>0)
    {
        // clear the existing list
        if ([self deleteCourses])
        {
            int loop;
            for (loop=0; loop<list.count; loop++)
            {
                [self saveCourseItem:[list objectAtIndex:loop]];
            }
            success = TRUE;
        }
    }
    return success;
}

- (BOOL) syncUniversityList:(NSString*)url
{
    WebserviceUtils *webservice = [[WebserviceUtils alloc] init];
    BOOL success=FALSE;
    url = [NSString stringWithFormat:@"%@%@", [self getServerName], url];
    NSMutableArray *list = [webservice downloadUniversityList:url:[self getProviderId]:[self getProviderPassword]];
    if (list!=nil && list.count>0)
    {
        // clear the existing list
        if ([self deleteUniversities])
        {
            int loop;
            for (loop=0; loop<list.count; loop++)
            {
                [self saveUniversityItem:[list objectAtIndex:loop]];
            }
            success = TRUE;
        }
    }
    return success;
}

- (BOOL) syncEventList:(NSString*)url
{
    WebserviceUtils *webservice = [[WebserviceUtils alloc] init];
    BOOL success=FALSE;
    url = [NSString stringWithFormat:@"%@%@", [self getServerName], url];
    NSMutableArray *list = [webservice downloadEventList:url:[self getProviderId]:[self getProviderPassword]];
    
    if (list!=nil && list.count>0)
    {
        int loop;
        for (loop=0; loop<list.count; loop++)
        {
            [self saveEventItem:[list objectAtIndex:loop]];
        }
        
        success = TRUE;
    }
    return success;
}

- (BOOL) uploadCandidates:(int)eventId
{
    WebserviceUtils *webservice = [[WebserviceUtils alloc] init];
    NSMutableArray *candidateList = [self loadCandidateList:eventId];
    BOOL success = false;
    
    if (candidateList!=nil && candidateList.count>0)
    {
        success = true;
        int loop;
        for (loop=0; loop<candidateList.count; loop++)
        {
            CandidateDetails *details = [candidateList objectAtIndex:loop];
            if (details!=nil && details.uploadStatus==0)
            {
                int response = [webservice registerCandidate:[self getServerName]:details:[self getProviderId]:[self getProviderPassword]];
                if (response==1)
                {
                    // update the candidate item as being uploaded successfully
                    details.uploadDate = [NSDate date];
                    details.uploadStatus = 1;
                    [self updateCandidateDetails:details];
                }
                else
                {
                    // add an error entry
                    NSString *error = nil;
                    
                    if (response == 2)
                    {
                        error = [NSString stringWithFormat:@"Error saving candidate to server: Email: %@, First name: %@, Last name: %@, Response: Candidate already exists",details.email,details.firstName,details.lastName];
                    }
                    else
                    {
                    error = [NSString stringWithFormat:@"Error saving candidate to server: Email: %@, First name: %@, Last name: %@, Response: %d",details.email,details.firstName,details.lastName,response];
                    }
                    [self saveError:error];
                }
            }
        }        
    }
    
    return success;
   
}

-(BOOL)saveCandidateRegistration: (int) eventId:(int) universityId:(int) courseId:(NSString*)firstName:(NSString*) lastName:(NSString*) emailAddress: (int) graduationYear
{
    bool state = false;
    sqlite3 *database = [DataUtils getNewDBConnection];
    
    if (database == nil) return state;
    
    NSString *query = [NSString stringWithFormat: @"INSERT INTO Registrations (EventId, UniversityId, CourseId, GraduationYear, FirstName, LastName, Email) values (%d,%d,%d,%d,'%@','%@','%@')", eventId, universityId, courseId, graduationYear, firstName, lastName, emailAddress];

    sqlite3_stmt *statement;
    if (sqlite3_prepare_v2(database, [query UTF8String], -1, &statement, nil) == SQLITE_OK)
    {
        if (SQLITE_DONE != sqlite3_step(statement))
            state = false;
        else
            state = true;
    }
    else
    {
        state = false;
            [self saveError: [NSString stringWithFormat: @"Error: failed to get save registration %s.", sqlite3_errmsg(database)]];
        
    }
    sqlite3_finalize(statement);
    sqlite3_close(database);
    return state;    
}

- (BOOL)updateCandidateDetails:(CandidateDetails*)details
{
    bool state = false;
    sqlite3 *database = [DataUtils getNewDBConnection];
    
    if (database == nil) return state;
    
    NSString *query = [NSString stringWithFormat: @"UPDATE Registrations SET EventId=%d, UniversityId=%d, CourseId=%d, GraduationYear=%d, FirstName='%@', LastName='%@', Email='%@', UploadStatus=%d, UploadDate='%@' WHERE ID=%d", details.eventId, details.universityId, details.courseId, details.graduationYear, details.firstName, details.lastName, details.email, details.uploadStatus, details.uploadDate, details.rowId];
    
    sqlite3_stmt *statement;
    if (sqlite3_prepare_v2(database, [query UTF8String], -1, &statement, nil) == SQLITE_OK)
    {
        if (SQLITE_DONE != sqlite3_step(statement))
            state = false;
        else
            state = true;
    }
    else
    {
        state = false;
        [self saveError: [NSString stringWithFormat: @"Error: failed to update registration %s.", sqlite3_errmsg(database)]];
        
    }
    sqlite3_finalize(statement);
    sqlite3_close(database);
    return state;    
}


- (CandidateDetails *)loadCandidate:(int)candidateId
{
    CandidateDetails *details = [[CandidateDetails alloc] init];
    sqlite3 *database = [DataUtils getNewDBConnection];
    if (database == nil) return nil;
    
    NSString *query = [NSString stringWithFormat: @"SELECT r.Id, r.EventId, r.UniversityId, r.CourseId, r.FirstName, r.LastName, r.Email, r.DateCreated, r.UploadStatus, r.GraduationYear, u.Description, c.Description FROM Registrations r left outer join Universities u on u.id=r.UniversityId left outer join Courses c on c.id=r.CourseId WHERE r.id = %d",candidateId];
    
    sqlite3_stmt *statement;
    
    if (sqlite3_prepare_v2(database, [query UTF8String], -1, &statement, nil) == SQLITE_OK)
    {
        while (sqlite3_step(statement)==SQLITE_ROW) {
            
            details.rowId = sqlite3_column_int(statement, 0);
            details.eventId = sqlite3_column_int(statement, 1);
            details.universityId = sqlite3_column_int(statement, 2);
            details.courseId = sqlite3_column_int(statement, 3);
            
            NSString *firstName = [NSString stringWithUTF8String: (char *)sqlite3_column_text(statement, 4)];
            [details setFirstName:firstName];
            
            NSString *lastName = [NSString stringWithUTF8String: (char *)sqlite3_column_text(statement, 5)];
            [details setLastName:lastName];
            
            NSString *email = [NSString stringWithUTF8String: (char *)sqlite3_column_text(statement, 6)];
            [details setEmail:email];
            
            NSString *dateCreatedStr = [NSString stringWithUTF8String: (char *)sqlite3_column_text(statement, 7)];
            
            // Convert string to date object
            NSDateFormatter *dateCreatedFormat = [[NSDateFormatter alloc] init];
            [dateCreatedFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
            
            NSDate *dateCreated = [dateCreatedFormat dateFromString:dateCreatedStr];  
            
            details.dateCreated = dateCreated;
            
            details.uploadStatus = sqlite3_column_int(statement, 8);
            details.graduationYear = sqlite3_column_int(statement, 9);
            
            if (sqlite3_column_text(statement, 10)!=nil)
            {
                NSString *universityName = [NSString stringWithUTF8String: (char *)sqlite3_column_text(statement, 10)];
                [details setUniversityName:universityName];
            }
            else
                [details setUniversityName:@"University not specified"];
            
            if (sqlite3_column_text(statement, 11)!=nil)
            {
                NSString *courseName = [NSString stringWithUTF8String: (char *)sqlite3_column_text(statement, 11)];
                [details setCourseName:courseName];
            }
            else
                [details setCourseName:@"Course not specified"];
            
            NSString *dateUploadedStr = [NSString stringWithUTF8String: (char *)sqlite3_column_text(statement, 7)];
            
            // Convert string to date object
            NSDateFormatter *dateUploadedFormat = [[NSDateFormatter alloc] init];
            [dateUploadedFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
            
            NSDate *dateUploaded = [dateUploadedFormat dateFromString:dateUploadedStr];  
            
            details.uploadDate = dateUploaded;
        }
        
    }
    else
    {
        [self saveError: [NSString stringWithFormat: @"Error: failed to get candidate list %s.", sqlite3_errmsg(database)]];
    }
    return details;
}


-(NSMutableArray*)loadCourseList
{
    sqlite3 *database = [DataUtils getNewDBConnection];
    if (database == nil) return nil;
    
    NSString *query = @"SELECT Id, Description FROM Courses ORDER BY Description";
    
    sqlite3_stmt *statement;
    NSMutableArray *array = [[NSMutableArray alloc] init];
    
    if (sqlite3_prepare_v2(database, [query UTF8String], -1, &statement, nil) == SQLITE_OK)
    {
        while (sqlite3_step(statement)==SQLITE_ROW) {
            
            PopOverListItem *item = [[PopOverListItem alloc] init];
            item.rowId = sqlite3_column_int(statement, 0);
            NSString *description = [NSString stringWithUTF8String: (char *)sqlite3_column_text(statement, 1)];
            [item setDescription:description];
            [array addObject:item];
            
        }
        
    }
    else
    {
        [self saveError: [NSString stringWithFormat: @"Error: failed to get course list %s.", sqlite3_errmsg(database)]];
    }
    sqlite3_finalize(statement);
    sqlite3_close(database);
    return array;
    
}


- (PopOverListItem *)loadCourseItem:(int)courseId
{
    PopOverListItem *item = [[PopOverListItem alloc] init];
    sqlite3 *database = [DataUtils getNewDBConnection];
    if (database == nil) return nil;
    
    NSString *query = [NSString stringWithFormat: @"SELECT Id, Description FROM Courses WHERE Id=%d",courseId];
    
    sqlite3_stmt *statement;
    
    if (sqlite3_prepare_v2(database, [query UTF8String], -1, &statement, nil) == SQLITE_OK)
    {
        while (sqlite3_step(statement)==SQLITE_ROW) {
            item.rowId = sqlite3_column_int(statement, 0);
            NSString *courseName = [NSString stringWithUTF8String: (char *)sqlite3_column_text(statement, 1)];
            [item setDescription: courseName];
        }
    }
    
    sqlite3_finalize(statement);
    sqlite3_close(database);
    
    return item;
}

- (BOOL)saveCourseItem:(PopOverListItem*) courseItem
{
    bool state = false;
    sqlite3 *database = [DataUtils getNewDBConnection];
    
    if (database == nil) return state;
    
    NSString *query;
    
    PopOverListItem *oldCourseItem = [self loadCourseItem:courseItem.rowId];
    
    if (oldCourseItem == nil || oldCourseItem.rowId==0)
    {
        query = [NSString stringWithFormat: @"INSERT INTO Courses (Id, Description) values (%d,'%@')", courseItem.rowId, courseItem.description ];
    }
    else
    {
        query = [NSString stringWithFormat: @"UPDATE Courses SET Description = '%@' WHERE Id=%d", courseItem.description, courseItem.rowId ];
        

    }
    sqlite3_stmt *statement;
    if (sqlite3_prepare_v2(database, [query UTF8String], -1, &statement, nil) == SQLITE_OK)
    {
        if (SQLITE_DONE != sqlite3_step(statement))
            state = false;
        else
            state = true;
    }
    else
    {
        state = false;
    }
    sqlite3_finalize(statement);
    sqlite3_close(database);
    return state; 
}

- (BOOL)deleteCourses
{
    bool state = false;
    sqlite3 *database = [DataUtils getNewDBConnection];
    
    if (database == nil) return state;
    
    NSString *query;
    query = [NSString stringWithFormat: @"DELETE FROM Courses" ];
        
    sqlite3_stmt *statement;
    if (sqlite3_prepare_v2(database, [query UTF8String], -1, &statement, nil) == SQLITE_OK)
    {
        if (SQLITE_DONE != sqlite3_step(statement))
            state = false;
        else
            state = true;
    }
    else
    {
        state = false;
    }
    sqlite3_finalize(statement);
    sqlite3_close(database);
    return state; 
}

- (NSMutableArray *)loadCandidateList:(int)eventId
{
    sqlite3 *database = [DataUtils getNewDBConnection];
    if (database == nil) return nil;
    
    NSString *query = [NSString stringWithFormat: @"SELECT r.Id, r.EventId, r.UniversityId, r.CourseId, r.FirstName, r.LastName, r.Email, r.DateCreated, r.UploadStatus, r.GraduationYear, u.Description, c.Description FROM Registrations r left outer join Universities u on u.id=r.UniversityId left outer join Courses c on c.id=r.CourseId WHERE r.EventId = %d",eventId];
    
    sqlite3_stmt *statement;
    NSMutableArray *array = [[NSMutableArray alloc] init];
    
    if (sqlite3_prepare_v2(database, [query UTF8String], -1, &statement, nil) == SQLITE_OK)
    {
        while (sqlite3_step(statement)==SQLITE_ROW) {
            
            CandidateDetails *item = [[CandidateDetails alloc] init];
            item.rowId = sqlite3_column_int(statement, 0);
            item.eventId = sqlite3_column_int(statement, 1);
            item.universityId = sqlite3_column_int(statement, 2);
            item.courseId = sqlite3_column_int(statement, 3);
            
            NSString *firstName = [NSString stringWithUTF8String: (char *)sqlite3_column_text(statement, 4)];
            [item setFirstName:firstName];

            NSString *lastName = [NSString stringWithUTF8String: (char *)sqlite3_column_text(statement, 5)];
            [item setLastName:lastName];

            NSString *email = [NSString stringWithUTF8String: (char *)sqlite3_column_text(statement, 6)];
            [item setEmail:email];

            NSString *dateCreatedStr = [NSString stringWithUTF8String: (char *)sqlite3_column_text(statement, 7)];
            
            // Convert string to date object
            NSDateFormatter *dateCreatedFormat = [[NSDateFormatter alloc] init];
            [dateCreatedFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
            
            NSDate *dateCreated = [dateCreatedFormat dateFromString:dateCreatedStr];  
            
            item.dateCreated = dateCreated;
            
            item.uploadStatus = sqlite3_column_int(statement, 8);
            item.graduationYear = sqlite3_column_int(statement, 9);

            if (sqlite3_column_text(statement, 10)!=nil)
            {
                NSString *universityName = [NSString stringWithUTF8String: (char *)sqlite3_column_text(statement, 10)];
                [item setUniversityName:universityName];
            }
            else
                [item setUniversityName:@"University not specified"];

            if (sqlite3_column_text(statement, 11)!=nil)
            {
                NSString *courseName = [NSString stringWithUTF8String: (char *)sqlite3_column_text(statement, 11)];
                [item setCourseName:courseName];
            }
            else
                [item setCourseName:@"Course not specified"];

            NSString *dateUploadedStr = [NSString stringWithUTF8String: (char *)sqlite3_column_text(statement, 7)];
            
            // Convert string to date object
            NSDateFormatter *dateUploadedFormat = [[NSDateFormatter alloc] init];
            [dateUploadedFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
            
            NSDate *dateUploaded = [dateUploadedFormat dateFromString:dateUploadedStr];  
            
            item.uploadDate = dateUploaded;
            [array addObject:item];
            
        }
        
    }
    else
    {
        [self saveError: [NSString stringWithFormat: @"Error: failed to get candidate list %s.", sqlite3_errmsg(database)]];
    }
    sqlite3_finalize(statement);
    sqlite3_close(database);
    return array;

}

- (BOOL)saveError:(NSString*) errorDescription
{
    bool state = false;
    sqlite3 *database = [DataUtils getNewDBConnection];
    
    if (database == nil) return state;
    
    NSString *query = [NSString stringWithFormat: @"INSERT INTO ErrorLog (Details) values ('%@')", errorDescription];
    
    sqlite3_stmt *statement;
    if (sqlite3_prepare_v2(database, [query UTF8String], -1, &statement, nil) == SQLITE_OK)
    {
        if (SQLITE_DONE != sqlite3_step(statement))
            state = false;
        else
            state = true;
    }
    else
    {
        state = false;
        NSString *errorMessage = [NSString stringWithFormat: @"Error: failed to get error list %s.", sqlite3_errmsg(database)];
        NSLog(@"%@",errorMessage);
    }

    sqlite3_finalize(statement);
    sqlite3_close(database);
    return state; 
}

-(NSMutableArray *) loadErrorList
{
    sqlite3 *database = [DataUtils getNewDBConnection];
    if (database == nil) return nil;
    
    NSString *query = @"SELECT ID, DateCreated, Details FROM ErrorLog ORDER BY DateCreated DESC";
    
    sqlite3_stmt *statement;
    NSMutableArray *array = [[NSMutableArray alloc] init];
    
    if (sqlite3_prepare_v2(database, [query UTF8String], -1, &statement, nil) == SQLITE_OK)
    {
        while (sqlite3_step(statement)==SQLITE_ROW) {
            
            ErrorLogItem *errorItem = [[ErrorLogItem alloc] init];
            errorItem.rowId = sqlite3_column_int(statement, 0);
            
            // Convert string to date object
            NSString *dateStr = [NSString stringWithUTF8String: (char *)sqlite3_column_text(statement, 1)];
            NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
            [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
            
            NSDate *date = [dateFormat dateFromString:dateStr];  
            [errorItem setErrorDate: date];
            
            
            NSString *errorDetails = [NSString stringWithUTF8String: (char *)sqlite3_column_text(statement, 2)];
            [errorItem setErrorDescription: errorDetails];
            
            [array addObject:errorItem];
            
        }
    }
    else
    {
        [self saveError: [NSString stringWithFormat: @"Error: failed to get error list %s.", sqlite3_errmsg(database)]];
    }
    sqlite3_finalize(statement);
    sqlite3_close(database);
    return array;
}

- (BOOL)clearErrorList
{
    bool state = false;
    sqlite3 *database = [DataUtils getNewDBConnection];
    
    if (database == nil) return state;
    
    NSString *query = @"DELETE FROM ErrorLog";
    
    sqlite3_stmt *statement;
    if (sqlite3_prepare_v2(database, [query UTF8String], -1, &statement, nil) == SQLITE_OK)
    {
        if (SQLITE_DONE != sqlite3_step(statement))
            state = false;
        else
            state = true;
    }
    else
    {
        state = false;
    }
    sqlite3_finalize(statement);
    sqlite3_close(database);
    return state;   
}


- (NSString*)getProviderId
{
    NSString *serverId = [KeyChain getStringForKey:@"milkround-provider-id"];
    if (serverId == nil)
    {
        [self setProviderId:@"1":@"DD208A11-2A89-4b6a-A42D-C83255775D40"];
        serverId = [KeyChain getStringForKey:@"milkround-provider-id"];
    }
    return serverId;
}


- (NSString*)getProviderPassword
{
    NSString *serverId = [KeyChain getStringForKey:@"milkround-provider-password"];
    if (serverId == nil)
    {
        serverId = @"";
    }
    return serverId;
}

- (BOOL)setProviderId:(NSString*)providerId:(NSString*)providerPassword;
{
    [KeyChain setString:providerId forKey:@"milkround-provider-id"];
    [KeyChain setString:providerPassword forKey:@"milkround-provider-password"];
    return true;
}

- (BOOL)checkPassword:(NSString*)password
{
    NSString* standardPassword = [KeyChain getStringForKey:@"milkround-standard-user"];
    NSString* adminPassword = [KeyChain getStringForKey:@"milkround-it-user"];
    
    // first time software is run, so need to create initial passwords
    if (standardPassword==nil)
    {
        [self resetPassword:FALSE];
        standardPassword = [KeyChain getStringForKey:@"milkround-standard-user"];
    }
    
    if (adminPassword==nil)
    {
        [self resetPassword:TRUE];
        adminPassword = [KeyChain getStringForKey:@"milkround-it-user"];
    }
    
    if ([standardPassword caseInsensitiveCompare:password] == NSOrderedSame)
    {
        return TRUE;
    }
    else if ([adminPassword caseInsensitiveCompare:password] == NSOrderedSame)
    {
        return TRUE;
    }
    
    return FALSE;
}

-(void)resetPassword:(BOOL)isITAdmin
{
    if (isITAdmin)
        [KeyChain setString:@"02030034059" forKey:@"milkround-it-user"];
    else
        [KeyChain setString:@"milkround" forKey:@"milkround-standard-user"];
}

-(BOOL)saveNewPassword:(NSString*)oldPassword:(NSString*)newPassword
{
    // first check that the password is for standard user
    NSString* standardPassword = [KeyChain getStringForKey:@"milkround-standard-user"];
    if ([standardPassword caseInsensitiveCompare:oldPassword] != NSOrderedSame)
    {
        // now check if that failed. Was user entering the IT ADMIN password (for resetting the standard user password?
        NSString* adminPassword = [KeyChain getStringForKey:@"milkround-it-user"];
        if ([adminPassword caseInsensitiveCompare:oldPassword] != NSOrderedSame)
            return FALSE;
    }
    [KeyChain setString:newPassword forKey:@"milkround-standard-user"];
    return TRUE;    
}

- (NSString*)getServerName
{
    return [KeyChain getStringForKey:@"server-name"];
}

- (BOOL)setServerName:(NSString*)serverName
{
    [KeyChain setString:serverName forKey:@"server-name"];
    return true;
}

@end
