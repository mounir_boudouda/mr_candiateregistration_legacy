//
//  FeedbackMessageOverlay.m
//  Candidate Registrations
//
//  Created by Robert Taylor on 03/08/2011.
//  Copyright 2011 Home. All rights reserved.
//

#import "FeedbackMessageOverlay.h"
#import "QuartzCore/QuartzCore.h"

@implementation FeedbackMessageOverlay

@synthesize closeButton;
@synthesize imageView;
@synthesize contentTextView;
@synthesize titleLabel;
@synthesize title;
@synthesize message;
@synthesize showCloseButton;
@synthesize autoCloseTimeout;
@synthesize overlayView;
@synthesize timer;
@synthesize containerView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    UIColor *bg = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.5];
    self.view.backgroundColor = bg;
    
    if (title != nil && title.length > 0)
        [titleLabel setText:title];
    
    if (message != nil && message.length > 0)
        [contentTextView setText:message];
    
    if (showCloseButton)
    {
        closeButton.hidden = false;
        //overlayView.frame = CGRectMake(192, 387, 385, 230);
    }
    else
    {
        closeButton.hidden = true;
        //overlayView.frame = CGRectMake(192, 387, 385, 200);    
    }
    
    if (autoCloseTimeout>0)
    {
        //set the timer
        self.timer = [NSTimer scheduledTimerWithTimeInterval:autoCloseTimeout 
                                                      target:self 
                                                    selector:@selector(timerCalled) 
                                                    userInfo:nil 
                                                     repeats:NO];
    }
    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
	return NO;
}

-(IBAction)closeButtonPressed:(id)sender
{
    [self closeOverlay];
}

-(void)closeOverlay
{
    // get the current view
    UIView *currentView = self.view;
    
    // remove the current view and replace with myView1
	[currentView removeFromSuperview];
    
    // set up an animation for the transition between the views
	CATransition *animation = [CATransition animation];
	[animation setDuration:0.5];
	[animation setType:kCATransitionFade];
	[animation setSubtype:kCATransitionFromLeft];
	[animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
	
	[[containerView layer] addAnimation:animation forKey:@"CloseMessageOverlay"];
 
}

-(void)timerCalled
{
    [self.timer invalidate];
    self.timer = nil; // ensures we never invalidate an already invalid Timer
    
    [self closeOverlay];
}


@end
