//
//  ErrorLogCellView.h
//  Candidate Registrations
//
//  Created by Robert Taylor on 13/08/2011.
//  Copyright 2011 Home. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ErrorLogItem.h"
@interface ErrorLogCellView : UITableViewCell
{
    UILabel *dateLabel;
    UILabel *descriptionLabel;
    ErrorLogItem *errorLogItem;
}

@property (nonatomic, strong) IBOutlet UILabel *dateLabel;
@property (nonatomic, strong) IBOutlet UILabel *descriptionLabel;
@property (nonatomic, strong) ErrorLogItem *errorLogItem;

-(void)initError;

@end
