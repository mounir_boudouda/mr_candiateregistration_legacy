//
//  SettingsViewController.m
//  Candidate Registrations
//
//  Created by Robert Taylor on 17/07/2011.
//  Copyright 2011 Home. All rights reserved.
//

#import "SettingsViewController.h"
#import "DataUtils.h"

@implementation SettingsViewController

@synthesize passwordOldTextField;
@synthesize passwordNewTextField;
@synthesize repeatPasswordTextField;
@synthesize syncEventsButton;
@synthesize providerIdTextField;
@synthesize providerPasswordTextField;
@synthesize syncCoursesButton;
@synthesize syncUniversitiesButton;
@synthesize changePasswordButton;
@synthesize changeServerIdButton;
@synthesize messageOverlay;
@synthesize changeBackgroundButton;
@synthesize backgroundImageTextField;
@synthesize isDownloadSuccessful;
@synthesize isDataSyncSuccessful;
@synthesize changeServerButton;
@synthesize changeServerTextField;
@synthesize uniListServiceTextField;
@synthesize courseListServiceTextField;
@synthesize eventListServiceTextField;
@synthesize currentServerLabel;
@synthesize currentTextField;
@synthesize lblVersionNumber;
@synthesize lblBuildNumber;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        isDownloadSuccessful = FALSE;
        isDataSyncSuccessful = FALSE;
        // load the default values
        
        
    }
    return self;
}


- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

-(void) setupCurrentServerLabel
{
    DataUtils *dataUtils = [[DataUtils alloc] init];
    [currentServerLabel setText: [dataUtils getServerName]];
    [changeServerTextField setText: [dataUtils getServerName]];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    currentTextField = textField;
}

-(BOOL) webFileExists :(NSString*)imageUrl
{
    NSURLRequest* request = [NSURLRequest requestWithURL:[NSURL URLWithString:imageUrl] cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:5.0];
    NSHTTPURLResponse* response = nil;
    NSError* error = nil;
    [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSLog(@"statusCode = %d", [response statusCode]);
    
    if ([response statusCode] != 200)
    {
        DataUtils *dataUtils = [[DataUtils alloc] init];
        [dataUtils saveError:[NSString stringWithFormat:@"Requested file %@ not found - Error code %d", imageUrl, [response statusCode]]];

        return NO;
    }
    else
        return YES;
}
- (void)startBackgroundImageDownload {
    
    @autoreleasepool {
    // do the background task
        isDownloadSuccessful = [self downloadBackgroundImage];
        
        [self performSelectorOnMainThread:@selector(downloadBackgroundImageDone) withObject:nil waitUntilDone:NO];
    }
    
}

- (void)downloadBackgroundImageDone {
    
    // close this view
    [self.messageOverlay closeOverlay];
    
    if (isDownloadSuccessful==FALSE)
    {
        FeedbackMessageOverlay *messageController = [[FeedbackMessageOverlay alloc] 
                                                     initWithNibName:@"FeedbackMessageOverlay" 
                                                     bundle:nil];
        messageController.showCloseButton = TRUE;
        messageController.title = @"Oops!";
        messageController.message = @"Image could not be saved";  
        messageController.containerView = self.view;
        self.messageOverlay = messageController;
        [self.view addSubview:messageOverlay.view];
    }
    else
    {
        FeedbackMessageOverlay *messageController = [[FeedbackMessageOverlay alloc] 
                                                     initWithNibName:@"FeedbackMessageOverlay" 
                                                     bundle:nil];
        messageController.showCloseButton = TRUE;
        messageController.title = @"Success!";
        messageController.message = @"Image uploaded successfully";
        messageController.containerView = self.view;
        self.messageOverlay = messageController;
        [self.view addSubview:messageOverlay.view];
    }
}

- (BOOL)downloadBackgroundImage
{
    NSString* imageUrl = backgroundImageTextField.text;
    NSURL *imageNSUrl;
    //if ([imageUrl hasPrefix:@"http://"]) {
        imageNSUrl = [NSURL URLWithString:imageUrl];
    //} else {
    //    imageNSUrl = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@",imageUrl]];
    //}
    
    if ([self webFileExists:imageUrl]==FALSE)
    {
        return FALSE;
    }
    
	// Get an image from the URL below
	UIImage *image = [[UIImage alloc] initWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:imageUrl]]];
    
	// save the file into Document folder.
	NSString *docDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    
	NSString *pngFilePath = [NSString stringWithFormat:@"%@/background.png",docDir];
    
    // first delete existing file if exists
    NSFileManager *filemgr;
    filemgr = [NSFileManager defaultManager];
    if ([filemgr fileExistsAtPath: pngFilePath ] == YES)
    {
        NSLog (@"File exists");
        if ([filemgr removeItemAtPath: pngFilePath error: NULL]  == YES)
            NSLog (@"Remove successful");
        else
        {
            DataUtils *dataUtils = [[DataUtils alloc] init];
            [dataUtils saveError:@"Removal of background image failed"];
            return FALSE;
        }
    }
    
	NSData *data1 = [NSData dataWithData:UIImagePNGRepresentation(image)];
	[data1 writeToFile:pngFilePath atomically:YES];
    
    return TRUE;
}

- (IBAction)changeBackgroundButtonClicked:(id)sender
{
    [self.view endEditing:YES];
    if (currentTextField!=nil)
        [currentTextField resignFirstResponder];
    BOOL problemsFound = false;
    NSString *errorString = @"";
    NSString *trimmedString = [NSString stringWithString:backgroundImageTextField.text];
    trimmedString = [trimmedString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
     if (![trimmedString hasPrefix:@"http://"]) {
        trimmedString = [NSString stringWithFormat:@"http://%@",trimmedString];
        [backgroundImageTextField setText:trimmedString];
    }
    
    if ([trimmedString caseInsensitiveCompare:@""] == NSOrderedSame)
    {
        errorString = [errorString stringByAppendingString:@"You must enter a url!.\n"];
        problemsFound = TRUE;
    }
    
    if (problemsFound)
    {
        FeedbackMessageOverlay *messageController = [[FeedbackMessageOverlay alloc] 
                                                     initWithNibName:@"FeedbackMessageOverlay" 
                                                     bundle:nil];
        messageController.showCloseButton = TRUE;
        messageController.title = @"Oops!";
        messageController.message = errorString;
        messageController.containerView = self.view;
        self.messageOverlay = messageController;
        [self.view addSubview:messageOverlay.view];
        
    }
    else
    {
        FeedbackMessageOverlay *messageController = [[FeedbackMessageOverlay alloc] 
                                                     initWithNibName:@"FeedbackMessageOverlay" 
                                                     bundle:nil];
        messageController.showCloseButton = FALSE;
        messageController.title = @"Downloading...";
        messageController.message = @"Please wait whilst the image is downloaded.";
        messageController.containerView = self.view;
       self.messageOverlay = messageController;
        
        [backgroundImageTextField setText:trimmedString];
        
        [self.view addSubview:messageOverlay.view];
        [NSThread detachNewThreadSelector:@selector(startBackgroundImageDownload) toTarget:self withObject:nil];

    }
}
- (IBAction)changePasswordButtonClicked:(id)sender
{
    [self.view endEditing:YES];
    if (currentTextField!=nil)
        [currentTextField resignFirstResponder];
    BOOL problemsFound = false;
    NSString *errorString = @"";
    NSString *trimmedString = [NSString stringWithString:passwordNewTextField.text];
    trimmedString = [trimmedString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    if ([trimmedString caseInsensitiveCompare:@""] == NSOrderedSame || [repeatPasswordTextField.text caseInsensitiveCompare:@""] == NSOrderedSame || [passwordOldTextField.text caseInsensitiveCompare:@""] == NSOrderedSame)
    {
        errorString = [errorString stringByAppendingString:@"All password fields must be completed.\n"];
        problemsFound = true;
        
    }
    else if ([trimmedString caseInsensitiveCompare:repeatPasswordTextField.text] != NSOrderedSame)
    {
        errorString = [errorString stringByAppendingString:@"The new passwords don't match.\n"];
        problemsFound = true;
    }
    
    if (!problemsFound)
    {
        DataUtils *dataUtils = [[DataUtils alloc] init];
        BOOL success = [dataUtils saveNewPassword:passwordOldTextField.text :passwordNewTextField.text];
        
        if (!success)
        {
            problemsFound = TRUE;
            errorString =  [errorString stringByAppendingString:@"The new password could not be saved. Check your old password.\n"];
        }
    }
    
    if (problemsFound)
    {
        FeedbackMessageOverlay *messageController = [[FeedbackMessageOverlay alloc] 
                                                     initWithNibName:@"FeedbackMessageOverlay" 
                                                     bundle:nil];
        messageController.showCloseButton = TRUE;
        messageController.title = @"Oops!";
        messageController.message = errorString;
        messageController.containerView = self.view;
        self.messageOverlay = messageController;
        [self.view addSubview:messageOverlay.view];
    }
    else
    {
        FeedbackMessageOverlay *messageController = [[FeedbackMessageOverlay alloc] 
                                                     initWithNibName:@"FeedbackMessageOverlay" 
                                                     bundle:nil];
        messageController.showCloseButton = TRUE;
        messageController.title = @"Data saved";
        messageController.message = @"The new password has been saved.";
        messageController.containerView = self.view;
        self.messageOverlay = messageController;
        [self.view addSubview:messageOverlay.view];
        
        [passwordOldTextField setText:@""];
        [passwordNewTextField setText:@""];
        [repeatPasswordTextField setText:@""];
        
    }
}
- (IBAction)changeServerIdButtonClicked:(id)sender
{
    [self.view endEditing:YES];
    if (currentTextField!=nil)
        [currentTextField resignFirstResponder];
    BOOL problemsFound = false;
    NSString *errorString = @"";
    NSString *trimmedProviderIdString = [NSString stringWithString:providerIdTextField.text];
    NSString *trimmedProviderPasswordString = [NSString stringWithString:providerPasswordTextField.text];
    trimmedProviderIdString = [trimmedProviderIdString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if ([trimmedProviderIdString caseInsensitiveCompare:@""] == NSOrderedSame)
    {
        errorString = [errorString stringByAppendingString:@"New ID cannot be empty.\n"];
        problemsFound = true;
    }
    
    trimmedProviderPasswordString = [trimmedProviderPasswordString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if ([trimmedProviderPasswordString caseInsensitiveCompare:@""] == NSOrderedSame)
    {
        errorString = [errorString stringByAppendingString:@"New password cannot be empty.\n"];
        problemsFound = true;
    }
    
    if (!problemsFound)
    {
        DataUtils *dataUtils = [[DataUtils alloc] init];
        BOOL success = [dataUtils setProviderId:trimmedProviderIdString :trimmedProviderPasswordString];
        
        if (!success)
        {
            problemsFound = TRUE;
            errorString =  [errorString stringByAppendingString:@"The new server id could not be saved.\n"];
        }
    }

    if (problemsFound)
    {
        FeedbackMessageOverlay *messageController = [[FeedbackMessageOverlay alloc] 
                                                     initWithNibName:@"FeedbackMessageOverlay" 
                                                     bundle:nil];
        messageController.showCloseButton = TRUE;
        messageController.title = @"Oops!";
        messageController.message = errorString;
        messageController.containerView = self.view;
        self.messageOverlay = messageController;
        [self.view addSubview:messageOverlay.view];
    }
    else
    {
        FeedbackMessageOverlay *messageController = [[FeedbackMessageOverlay alloc] 
                                                     initWithNibName:@"FeedbackMessageOverlay" 
                                                     bundle:nil];
        messageController.showCloseButton = TRUE;
        messageController.title = @"Data saved";
        messageController.message = @"The new server id has been saved.";
        messageController.containerView = self.view;
        self.messageOverlay = messageController;
        [self.view addSubview:messageOverlay.view];
        
        [providerIdTextField setText:@""];
        [providerPasswordTextField setText:@""];
    }
}
    
- (IBAction)changeServerButtonClicked:(id)sender
{
    DataUtils *dataUtils = [[DataUtils alloc] init];
    
    [dataUtils setServerName:changeServerTextField.text];
    
    [currentServerLabel setText: [dataUtils getServerName]];
    
    if (currentTextField!=nil)
        [currentTextField resignFirstResponder];
    
    FeedbackMessageOverlay *messageController = [[FeedbackMessageOverlay alloc] 
                                                 initWithNibName:@"FeedbackMessageOverlay" 
                                                 bundle:nil];
    messageController.showCloseButton = TRUE;
    messageController.title = @"Success!";
    messageController.message = @"Server name was updated";
    messageController.containerView = self.view;
    self.messageOverlay = messageController;
    [self.view addSubview:messageOverlay.view];

}

- (IBAction)syncEventsButtonClicked:(id)sender
{
     if (currentTextField!=nil)
        [currentTextField resignFirstResponder];
    
    FeedbackMessageOverlay *messageController = [[FeedbackMessageOverlay alloc] 
                                                 initWithNibName:@"FeedbackMessageOverlay" bundle:nil];
    

    
    messageController.showCloseButton = FALSE;
    messageController.title = @"Downloading...";
    messageController.message = @"Please wait whilst the event list is downloaded.";
    messageController.containerView = self.view;
    self.messageOverlay = messageController;
    [self.view addSubview:messageOverlay.view];
    
    NSNumber* dataType = [NSNumber numberWithInt:1];
    [NSThread detachNewThreadSelector:@selector(startDataSync:) toTarget:self withObject:dataType];
}


- (IBAction)syncUniversitiesButtonClicked:(id)sender
{
    if (currentTextField!=nil)
        [currentTextField resignFirstResponder];
    
    FeedbackMessageOverlay *messageController = [[FeedbackMessageOverlay alloc]
                                                 initWithNibName:@"FeedbackMessageOverlay" 
                                                 bundle:nil];
    messageController.showCloseButton = FALSE;
    messageController.title = @"Downloading...";
    messageController.message = @"Please wait whilst the university list is downloaded.";
    messageController.containerView = self.view;
    self.messageOverlay = messageController;
    [self.view addSubview:messageOverlay.view];
    
    NSNumber* dataType = [NSNumber numberWithInt:2];
    [NSThread detachNewThreadSelector:@selector(startDataSync:) toTarget:self withObject:dataType]; 
}


- (IBAction)syncCoursesButtonClicked:(id)sender
{
    if (currentTextField!=nil)
        [currentTextField resignFirstResponder];
    
    FeedbackMessageOverlay *messageController = [[FeedbackMessageOverlay alloc]
                                                 initWithNibName:@"FeedbackMessageOverlay" 
                                                 bundle:nil];
    messageController.showCloseButton = FALSE;
    messageController.title = @"Downloading...";
    messageController.message = @"Please wait whilst the course list is downloaded.";
    self.messageOverlay = messageController;
    messageController.containerView = self.view;
    [self.view addSubview:messageOverlay.view];
    
    NSNumber* dataType = [NSNumber numberWithInt:3];
    [NSThread detachNewThreadSelector:@selector(startDataSync:) toTarget:self withObject:dataType];
}

-(void) startDataSync:(NSNumber*)dataType
{
    
    @autoreleasepool {
    
        DataUtils *dataUtils = [[DataUtils alloc] init];
        if ([dataType intValue] == 1)
            isDataSyncSuccessful = [dataUtils syncEventList:eventListServiceTextField.text];
        else if ([dataType intValue] == 2)
            isDataSyncSuccessful = [dataUtils syncUniversityList:uniListServiceTextField.text];
        else if ([dataType intValue] == 3)
            isDataSyncSuccessful = [dataUtils syncCourseList:courseListServiceTextField.text];
        
        [self performSelectorOnMainThread:@selector(dataSyncComplete) withObject:nil waitUntilDone:NO];
    }
    
    
}

-(void) dataSyncComplete
{
    // close this view
    [self.messageOverlay closeOverlay];
    
    if (isDataSyncSuccessful==FALSE)
    {
        FeedbackMessageOverlay *messageController = [[FeedbackMessageOverlay alloc] 
                                                     initWithNibName:@"FeedbackMessageOverlay" 
                                                     bundle:nil];
        messageController.showCloseButton = TRUE;
        messageController.title = @"Oops!";
        messageController.message = @"Data sync failed";            
        messageController.containerView = self.view;
        self.messageOverlay = messageController;
        [self.view addSubview:messageOverlay.view];
    }
    else
    {
        FeedbackMessageOverlay *messageController = [[FeedbackMessageOverlay alloc] 
                                                     initWithNibName:@"FeedbackMessageOverlay" 
                                                     bundle:nil];
        messageController.showCloseButton = TRUE;
        messageController.title = @"Success!";
        messageController.message = @"Data sync was successful";
        messageController.containerView = self.view;
        self.messageOverlay = messageController;
        [self.view addSubview:messageOverlay.view];
    }
    isDataSyncSuccessful = FALSE;
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    passwordOldTextField.delegate = self;
    passwordNewTextField.delegate = self;
    repeatPasswordTextField.delegate = self;
    backgroundImageTextField.delegate = self;
    providerIdTextField.delegate = self;
    providerPasswordTextField.delegate = self;
    uniListServiceTextField.delegate = self;
    eventListServiceTextField.delegate = self;
    courseListServiceTextField.delegate = self;
    changeServerTextField.delegate = self;
    [self setVersionNumber];
    [self setupCurrentServerLabel];
}

-(void)setVersionNumber {
    NSString * version = [[NSBundle mainBundle] objectForInfoDictionaryKey: @"CFBundleShortVersionString"];
    NSString * build = [[NSBundle mainBundle] objectForInfoDictionaryKey: (NSString *)kCFBundleVersionKey];
    [lblVersionNumber setText:[NSString stringWithFormat:@"%@", version]];
    [lblBuildNumber setText:[NSString stringWithFormat:@"%@", build]];

}

- (void)viewDidUnload
{
    lblVersionNumber = nil;
    [self setLblBuildNumber:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
	return NO;
}

@end
