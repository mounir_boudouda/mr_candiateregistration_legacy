//
//  ArchivedEventsViewController.h
//  Candidate Registrations
//
//  Created by Robert Taylor on 17/07/2011.
//  Copyright 2011 Home. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EventListTableViewController.h"

@interface ArchivedEventsViewController : UIViewController {
    EventListTableViewController *tableListController;
    UIView *tableViewContainerView;
}
@property (nonatomic, strong) IBOutlet EventListTableViewController *tableListController;
@property (nonatomic, strong) IBOutlet UIView *tableViewContainerView;
@end
