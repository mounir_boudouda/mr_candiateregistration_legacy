//
//  ErrorLogCellView.m
//  Candidate Registrations
//
//  Created by Robert Taylor on 13/08/2011.
//  Copyright 2011 Home. All rights reserved.
//

#import "ErrorLogCellView.h"

@implementation ErrorLogCellView
@synthesize descriptionLabel;
@synthesize dateLabel;
@synthesize errorLogItem;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)initError
{
    NSString *rowTitle = errorLogItem.errorDescription;
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd/MM/yy hh:mm:ss"];
    
    NSString *stringFromDate = [formatter stringFromDate:errorLogItem.errorDate];
    descriptionLabel.text = rowTitle;  
    dateLabel.text = stringFromDate;
}

@end
